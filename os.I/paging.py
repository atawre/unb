#!/usr/bin/python
import sys
from sys import stdin
from collections import deque
from pdb import set_trace

frames = int(sys.argv[1])
policy = sys.argv[2]
frames_queue = deque()  #frames

frame_to_ptable = {}  #hash map for frame to page table entry
psize = 1048576
pTable = []
pFault = 0
swapOut = 0
swapIn = 0

def replace_frames(iterator):
    global policy
    global frames_queue
    e = frames_queue[iterator]
    if policy=='l':
        del frames_queue[iterator]
        e[1] = 1     #frame acquired
        frames_queue.appendleft(e)
    elif policy=='f':
        if e[1]==0:
            e[1] = 1     #frame acquired
        else:
            e = frames_queue.popleft()
            frames_queue.append(e)

    return e

def get_frame(pIndex):
    global frames_queue
    global pTable
    global frame_to_ptable
    global swapOut
    global policy

    iterator = 0
    for e in frames_queue:
        frameId = e[0]
        frameStatus = e[1]
        if frameStatus==0:
            replace_frames(iterator)
            frame_to_ptable[frameId] = pIndex
            return frameId

        iterator += 1

    iterator -= 1
    #no free frames :( , lets replace in LRU order.
    e = replace_frames(iterator)
    frameId = e[0]

    old_pIndex = frame_to_ptable[frameId]
    (frameId, dirty) = pTable[old_pIndex]
    pTable[old_pIndex] = [-1, dirty]
    if dirty==1:
        swapOut += 1

    frame_to_ptable[frameId] = pIndex
    return frameId

def init_frames():
    global frames
    global frames_queue
    #frames are empty
    for i in range(0, frames):
        frames_queue.append([i, 0])

def init_ptable():
    #page table entries are empty
    for i in range(0, psize):
        pTable.append([-1, 0])      #frame number, dirty bit

init_frames()
init_ptable()

for line in stdin:
    (op, addr) = line.split()
    laddr  = int(addr)
    pIndex = laddr>>12
    offset = laddr&0xFFF

    (frame, dirty) = pTable[pIndex]
    if frame==-1:
        pFault += 1
        if dirty==1:
            swapIn += 1
        elif op in ['w', 'W']:
            dirty = 1
        frame = get_frame(pIndex)
    elif op in ['w', 'W']:
        dirty = 1

    pTable[pIndex] = [frame, dirty]
    frame = frame<<12
    paddr = frame | offset
    #print laddr, "->", paddr

#print "--------------------------------------"
print "Page faults : ", pFault
print "Swap Ins : ", swapIn
print "Swap Outs : ", swapOut
#print "--------------------------------------"


#!/usr/bin/python

from collections import deque
import sys
from sys import stdin

sector_time = {}
time_sector = {}
algo  = sys.argv[1]
start = int(sys.argv[2])
frames_queue = deque()  #frames

requests = deque()
requests.append([815 , 11])
requests.append([259 , 20])
requests.append([437 , 25])
requests.append([245 , 45])
requests.append([755 , 52])
requests.append([604 , 64])
requests.append([421 , 71])
requests.append([585 , 72])
requests.append([584 , 84])
requests.append([202 , 89])
requests.append([420 , 102])
requests.append([409 , 123])
requests.append([801 , 127])
requests.append([696 , 146])
requests.append([772 , 158])
requests.append([413 , 160])
requests.append([308 , 171])
requests.append([130 , 176])
requests.append([122 , 196])

def read_requests():
    global requests
    for line in stdin:
        (s, t)  = [ int(x) for x in line.split() ]
        requests.append([s, t])
        sector_time[s] = t
        time_sector[t] = s

def update():
    global requests
    for (s, t) in requests:
        sector_time[s] = t
        time_sector[t] = s

def cscan():
    global requests
    global prevSector
    global time

    if len(requests) == 0:
        return None, None

    minDistance = 2000
    nextSector  = -1
    reqTime  = -1

    pending = sorted([s for (s, t) in requests if t <= time and prevSector < s])
    future  = sorted([t for (s, t) in requests if t >  time and prevSector < s])

    if len(pending) > 0:
        nextSector = pending[0]
        reqTime = [t for (s, t) in requests if s==nextSector][0]
        print ""
    elif len(future) > 0:
        reqTime = future[0]
        nextSector = [s for (s, t) in requests if t==reqTime][0]
        print ""
    elif prevSector==999:
        nextSector = 0
    else:
        nextSector = 999

    if reqTime != -1:
        requests.remove([nextSector, reqTime])

    return (nextSector, reqTime)

def look():
    global requests
    global prevSector
    global time

    if len(requests) == 0:
        return None, None

    minDistance = 2000
    nextSector  = -1
    reqTime  = -1

    pending = sorted([s for (s, t) in requests if t <= time and prevSector < s])
    future  = sorted([t for (s, t) in requests if t >  time and prevSector < s])

    pending_backward = sorted([s for (s, t) in requests if t <= time and prevSector > s])
    future_backward  = sorted([s for (s, t) in requests if t >  time and prevSector > s])

    if len(pending) > 0:
        nextSector = pending[0]
        reqTime = [t for (s, t) in requests if s==nextSector][0]
        print ""
    elif len(future) > 0:
        reqTime = future[0]
        nextSector = [s for (s, t) in requests if t==reqTime][0]
        print ""
    elif len(pending_backward) > 0:
        nextSector = pending_backward[0]
        reqTime = [t for (s, t) in requests if s==nextSector][0]
        print ""
    elif len(future_backward) > 0:
        nextSector = future_backward[0]
        reqTime = [t for (s, t) in requests if s==nextSector][0]
        print ""

    #if reqTime != -1:
    requests.remove([nextSector, reqTime])

    return (nextSector, reqTime)


def fcfs():
    global requests
    if len(requests) == 0:
        return None, None
    return requests.popleft()

def sstf():
    global requests
    global prevSector
    global time

    if len(requests) == 0:
        return None, None

    minDistance = 2000
    nextSector  = -1
    reqTime     = 0

    pending = [s for (s, t) in requests if t <= time]
    future  = [t for (s, t) in requests if t > time]

    if len(pending) > 0:
        for s in pending:
            if abs(s-prevSector) < minDistance:
                minDistance = abs(s-prevSector)
                nextSector  = s
        reqTime = [t for (s, t) in requests if s==nextSector][0]
    else:
        reqTime = min(future)
        nextSector = [s for (s, t) in requests if t==reqTime][0]

    requests.remove([nextSector, reqTime])
    return (nextSector, reqTime)


def getNext():
    global algo
    if algo in ['f', 'F']:
        return fcfs()
    if algo in ['t', 'T']:
        return sstf()
    if algo in ['c', 'C']:
        return cscan()
    if algo in ['l', 'L']:
        return look()

#read_requests()
update()
prevSector  = start
prevTime    = 0
time        = 0
dist        = 0

(sector, arrival) = getNext()

if time < arrival:
    time = arrival  #wait for next requests.

dist = sector - prevSector

if dist < 0:
    direction = -1
else:
    direction = 1

#very first request doesn't account for direction change.
time += abs(dist)/10
(prevSector, prevTime) = (sector, arrival)

print sector
while True:
    (sector, arrival) = getNext()
    if sector==None:
        break

    if time < arrival:
        time = arrival  #wait for next requests.

    time += abs(sector - prevSector)/10
    dist += abs(sector - prevSector)

    if (sector - prevSector)*direction < 0:
        time      += 5
        direction *= -1
        print "change", sector
    else:
        print sector

    if arrival==-1:
        arrival = time
    (prevSector, prevTime) = (sector, arrival)

print "time =>", time, "distance =>", dist, "\n"



#!/usr/bin/python

import random
import sys

n=int(sys.argv[1])

for i in range(0, n):
    addr = random.randint(1, 4294967295)
    op = 'w'
    if i%5 :
        op = 'r'
    print op, addr
    print op, addr+1
    #print op, addr+2
    #print op, addr+3
    #print op, addr+4

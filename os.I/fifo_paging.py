#!/usr/bin/python
import sys
from sys import stdin
from collections import deque

frames = int(sys.argv[1])
fifo = deque()  #frames

frame_to_ptable = {}  #hash map for frame to page table entry
psize = 1048576
pTable = []
pFault = 0

def init_frames():
    global frames
    global fifo
    #frames are empty
    for i in range(0, frames):
        fifo.append([i, 0])

def get_frame(pIndex):
    global fifo
    global pTable
    global pFault

    for e in fifo:
        frameId = e[0]
        frameStatus = e[1]
        if frameStatus==0:
            e[1] = 1     #frame acquired
            frame_to_ptable[frameId] = pIndex
            return frameId

    #no free frames :( , lets replace in FIFO order.
    e = fifo[0]
    old_pIndex = frame_to_ptable[e[0]]
    pTable[old_pIndex] = -1

    e = fifo.popleft()
    frameId = e[0]
    fifo.append(e)
    return frameId


def init_ptable():
    #page table entries are empty
    for i in range(0, psize):
        pTable.append(-1)

init_frames()
init_ptable()

for line in stdin:
    laddr  = int(line)
    pIndex = laddr>>12
    offset = laddr&0xFFF
    if(pTable[pIndex]==-1):
        pFault += 1
        pTable[pIndex] = get_frame(pIndex)

    frame = pTable[pIndex]
    frame = frame<<12
    paddr = frame | offset
    print laddr, "->", paddr

print "Page faults : ", pFault


#include "monitor.h"
#include "test.h"
#include "task.h"

void testSetPriority(){
	if(0==(current_task->id%11))	//0
		setpriority(current_task->id, 1);
	else
		setpriority(current_task->id, current_task->id%11);
}

void testGetPriority(){
	print("PID : ");
    print_hex(current_task->id);
    print(" Priority : ");
    print_hex(getpriority(current_task->id));
    print("\n");
}

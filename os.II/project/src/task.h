//
// task.h - Defines the structures and prototypes needed to multitask.
//          Written for JamesM's kernel development tutorials.
//

#ifndef TASK_H
#define TASK_H

#include "common.h"
#include "paging.h"
#include "kheap.h"
#include "semaphore.h"
#include "pipe.h"


#define TASK_RUNNING 0
#define TASK_INTERRUPTIBLE_READY 1
#define TASK_INTERRUPTIBLE 2
#define TASK_WAITING 3
#define TASK_YIELD 4
#define TASK_ZOMBIE 5

#define MAX_OPEN_FILES 50
#define MAX_SEMAPHORE 20

typedef struct semaphore sem_t;


// This structure defines a 'task' - a process.
typedef struct task{
    int id;                // Process ID.
    u32int esp, ebp;       // Stack and base pointers.
    u32int eip;            // Instruction pointer.
    page_directory_t *page_directory; // Page directory.
    heap_t *heap;          // process specific heap.
    int priority;
    int original_priority;
    int state;
    int sleepInterval;
    int parent;
    u32int total_semaphore;
    u32int semlist[MAX_SEMAPHORE];
    sem_t *wait_semaphore;
    u32int *ftable;
    struct task *next;     // The next task in a linked list.
} task_t;

volatile task_t *current_task;
volatile task_t *ready_queue;
volatile task_t *wait_queue;

typedef struct pipe pipe_t;
extern pipe_t *pipelist;

// Initialises the tasking system.
void initialise_tasking();

// Called by the timer hook, this changes the running process.
void task_switch();

// Forks the current process, spawning a new one with a different
// memory space.
int fork();

// Causes the process to surrender the CPU. The result is that the process
//   with the highest priority is assigned the CPU. Note: This may be the
//   current process *if* it is the highest priority.
void yield();

// Causes the current process' stack to be forcibly moved to a new location.
void move_stack(void *new_stack_start, u32int size);

// Returns the pid of the current process.
int getpid();

int setpriority(int pid, int new_priority);

void insert_ordered_queue(task_t **queue, task_t *task);

task_t* remove_from_queue(task_t **queue, task_t *t);

void append(task_t **queue, task_t *task);

#endif

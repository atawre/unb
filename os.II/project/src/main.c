// main.c -- Defines the C-code kernel entry point, calls initialisation routines.
//           Made for JamesM's tutorials <www.jamesmolloy.co.uk>

#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

#define MAXPIPE_SIZE 1024
#define PAGE_SIZE 4096
#define MAX_PROCS 10    //safe number :).
extern u32int placement_address;
u32int initial_esp;
extern sem_t *semaphorelist;

int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    int pids[MAX_PROCS];
    int i;

    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();
    int *val=0;
    int pid;
    u32int pdata;

    int p = open_pipe();

    /* Start children. */
    for (i = 0; i < MAX_PROCS; ++i) {
      if ((pids[i] = fork()) < 0) {
        print("fork failed.");
        return(1);
      } else if (pids[i] == 0) {  //children
        int priority=1;
        pid = getpid();
        if(pid%11)
            priority = pid%11;
        else
            priority = 1;
        setpriority(pid, priority);
        if(pid==2){
            print("\n(pid:2)write "); print_dec(pid);
            ASSERT(sizeof(int)==write(p, &pid, sizeof(int)));
        }else if(pid==3){
            //sleep(1);
            print("\n(pid:3)write "); print_dec(pid);
            ASSERT(sizeof(int)==write(p, &pid, sizeof(int)));
        }else if(pid==4){
            print("\n(pid:4)write "); print_dec(pid);
            ASSERT(sizeof(int)==write(p, &pid, sizeof(int)));
        }else if(pid==6){
            sleep(3);
            ASSERT(sizeof(int)==read(p, &pdata, sizeof(int)));
            print("\nread  "); print_dec(pdata);

            ASSERT(sizeof(int)==read(p, &pdata, sizeof(int)));
            print("\nread  "); print_dec(pdata);

            ASSERT(sizeof(int)==read(p, &pdata, sizeof(int)));
            print("\nread  "); print_dec(pdata);

            sleep(4);

            print("\n=========== testing circular queue(pipe size = 12bytes) ===========");
            print("\n=========== MAXPIPE_SIZE can be set to 1k, 2k etc       ===========");
            int q = open_pipe();
            int qdata=0;
            
            print("\n(pid:6)write "); print_dec(pid);
            ASSERT(sizeof(int)==write(p, &pid, sizeof(int))); pid++;

            ASSERT(sizeof(int)==read(p, &qdata, sizeof(int)));
            print("\nread  "); print_dec(qdata);

            print("\n(pid:6)write "); print_dec(pid);
            ASSERT(sizeof(int)==write(p, &pid, sizeof(int))); pid++;

            print("\n(pid:6)write "); print_dec(pid);
            ASSERT(sizeof(int)==write(p, &pid, sizeof(int))); pid++;

            print("\n(pid:6)write "); print_dec(pid);
            ASSERT(sizeof(int)==write(p, &pid, sizeof(int))); pid++;

            ASSERT(sizeof(int)==read(p, &qdata, sizeof(int)));
            print("\nread  "); print_dec(qdata);

            ASSERT(sizeof(int)==read(p, &qdata, sizeof(int)));
            print("\nread  "); print_dec(qdata);

            ASSERT(sizeof(int)==read(p, &qdata, sizeof(int)));
            print("\nread  "); print_dec(qdata);
            close_pipe(q);
        }
        close_pipe(p);
        return(0);
      }
    }

    close_pipe(p);

    return 0;
}


//
// task.c - Implements the functionality needed to multitask.
//          Written for JamesM's kernel development tutorials.
//

#include "task.h"
#include "paging.h"
#include "monitor.h"
#include "kheap.h"

// Some externs are needed to access members in paging.c...
extern page_directory_t *kernel_directory;
extern page_directory_t *current_directory;
extern void alloc_frame(page_t*,int,int);
extern u32int initial_esp;
extern u32int read_eip();
extern u32int tick;
extern u32int frequency;
extern void perform_task_switch(u32int, u32int, u32int, u32int);

// The next available process ID.
u32int next_pid = 1;

task_t* remove_from_queue(task_t **queue, task_t *t){
  //should we disable interrupts here?
  asm volatile("cli");

  if(!*queue || !t)
    return 0;
  //removing the first process from the queue
  if(*queue==t){
    *queue  = t->next;
    t->next = 0;
    return t;
  }

  task_t *iterator=*queue;
  while(iterator->next && iterator->next!=t)
    iterator = iterator->next;

  //found.
  if(iterator->next==t){
    iterator->next = t->next;
    t->next = 0;
  }

  asm volatile("sti");
  return t;
}

void append(task_t **queue, task_t *task){

  if(!(*queue)){
    *queue = task;
    return;
  }

  task_t *iterator=*queue;

	while(iterator->next)
		iterator = iterator->next;
	iterator->next = task;
	task->next = 0;
}

void insert_ordered_queue(task_t **queue, task_t *task){
  task_t *tmp, *prev, *iterator;
  asm volatile("cli");
  iterator = (task_t *)*queue;
  prev = 0;
  while(iterator){
    if(iterator->priority > task->priority)
      break;
    prev = iterator;
    iterator = iterator->next;
  }

  tmp = 0;
  if(prev){
    tmp = prev->next;
    prev->next = task;
  }else{
    tmp = *queue;
    *queue = task;
  }
  task->next = tmp;
  asm volatile("sti");
}

void initialise_tasking()
{
    // Rather important stuff happening, no interrupts please!
    asm volatile("cli");

    // Relocate the stack so we know where it is.
    move_stack((void*)0xE0000000, 0x2000);

    // Initialise the first task (kernel task)
    current_task = ready_queue = (task_t*)kmalloc(sizeof(task_t));

    //initialise the open file table.
    current_task->ftable  = (u32int *)kmalloc(MAX_OPEN_FILES*sizeof(u32int));
    int i=0;
    for(; i<MAX_OPEN_FILES; i++){
      current_task->ftable[i] = 0;
    }
    current_task->ftable[0] = current_task->ftable[1] = current_task->ftable[2] = 1;

    current_task->id = next_pid++;
    current_task->priority = current_task->original_priority = 1;
    current_task->esp = current_task->ebp = 0;
    current_task->eip = 0;
    current_task->total_semaphore = 0;
    current_task->wait_semaphore = 0;
    // current_task->semaphores = (u32int *)kmalloc(10*sizeof(u32int));
    // for(int i=0; i<10; i++)
    //     current_task->semaphores[i] = -1;

    current_task->page_directory = current_directory;
    current_task->next = 0;

    wait_queue = 0; //wait queue is empty initially.
    // Reenable interrupts.
    asm volatile("sti");
}

void move_stack(void *new_stack_start, u32int size)
{
  u32int i;
  // Allocate some space for the new stack.
  for( i = (u32int)new_stack_start;
       i >= ((u32int)new_stack_start-size);
       i -= 0x1000)
  {
    // General-purpose stack is in user-mode.
    alloc_frame( get_page(i, 1, current_directory), 0 /* User mode */, 1 /* Is writable */ );
  }

  // Flush the TLB by reading and writing the page directory address again.
  u32int pd_addr;
  asm volatile("mov %%cr3, %0" : "=r" (pd_addr));
  asm volatile("mov %0, %%cr3" : : "r" (pd_addr));

  // Old ESP and EBP, read from registers.
  u32int old_stack_pointer; asm volatile("mov %%esp, %0" : "=r" (old_stack_pointer));
  u32int old_base_pointer;  asm volatile("mov %%ebp, %0" : "=r" (old_base_pointer));

  // Offset to add to old stack addresses to get a new stack address.
  u32int offset            = (u32int)new_stack_start - initial_esp;

  // New ESP and EBP.
  u32int new_stack_pointer = old_stack_pointer + offset;
  u32int new_base_pointer  = old_base_pointer  + offset;

  // Copy the stack.
  memcpy((void*)new_stack_pointer, (void*)old_stack_pointer, initial_esp-old_stack_pointer);

  // Backtrace through the original stack, copying new values into
  // the new stack.
  for(i = (u32int)new_stack_start; i > (u32int)new_stack_start-size; i -= 4)
  {
    u32int tmp = * (u32int*)i;
    // If the value of tmp is inside the range of the old stack, assume it is a base pointer
    // and remap it. This will unfortunately remap ANY value in this range, whether they are
    // base pointers or not.
    if (( old_stack_pointer < tmp) && (tmp < initial_esp))
    {
      tmp = tmp + offset;
      u32int *tmp2 = (u32int*)i;
      *tmp2 = tmp;
    }
  }

  // Change stacks.
  asm volatile("mov %0, %%esp" : : "r" (new_stack_pointer));
  asm volatile("mov %0, %%ebp" : : "r" (new_base_pointer));
}

void process_wait_queue(){
	task_t *prev, *next, *iterator = wait_queue;
	asm volatile("cli");
	while(iterator){
		if(iterator->sleepInterval <= tick){
			if(iterator==wait_queue){
				next = wait_queue = iterator->next;
			}else{
				next = prev->next = iterator->next;
				iterator->next = 0;
			}
      //iterator->priority = iterator->original_priority; //restore priority.
      insert_ordered_queue(&ready_queue, iterator);
      iterator->state = TASK_RUNNING;
			iterator = next;
		}else{
			prev = iterator;
			iterator = iterator->next;
		}
	}
	asm volatile("sti");
}

task_t *yield_from_queue(task_t *task){
    //increase_priority(&ready_queue);
    remove_from_queue(&ready_queue, task);
    task->state = TASK_RUNNING;	         //change it to RUNNING again.
    insert_ordered_queue(&ready_queue, task);
    // printQ(ready_queue);
    return ready_queue;
}

void switch_task(){
  // If we haven't initialised tasking yet, just return.
  if (!current_task)
	return;

  process_wait_queue();

  // Read esp, ebp now for saving later on.
  u32int esp, ebp, eip;
  asm volatile("mov %%esp, %0" : "=r"(esp));
  asm volatile("mov %%ebp, %0" : "=r"(ebp));

  // Read the instruction pointer. We do some cunning logic here:
  // One of two things could have happened when this function exits -
  //   (a) We called the function and it returned the EIP as requested.
  //   (b) We have just switched tasks, and because the saved EIP is essentially
  //       the instruction after read_eip(), it will seem as if read_eip has just
  //       returned.
  // In the second case we need to return immediately. To detect it we put a dummy
  // value in EAX further down at the end of this function. As C returns values in EAX,
  // it will look like the return value is this dummy value! (0x12345).
  eip = read_eip();

  // Have we just switched tasks?
  if (eip == 0x12345)
      return;

  // No, we didn't switch tasks. Let's save some register values and switch.
  current_task->eip = eip;
  current_task->esp = esp;
  current_task->ebp = ebp;

  // Get the next task to run.
  task_t *sleeping_task = 0;
  task_t *task_to_kill  = 0;
  task_t *yield_task    = 0;
  task_t *semwait_task  = 0;

  if(current_task->state==TASK_INTERRUPTIBLE_READY){
    sleeping_task = current_task;
  }else if(current_task->state==TASK_YIELD){ 		 //append to ready q.
    yield_task = yield_from_queue(current_task);  // returns next tasks to run.
  }else if(current_task->state==TASK_ZOMBIE){    // exit is setting task to ZOMBIE. Free it.
    task_to_kill = current_task;
  }else if(current_task->state==TASK_WAITING){
    semwait_task = current_task;
  }


  if(sleeping_task || yield_task || task_to_kill || semwait_task || !current_task->next){
    current_task = ready_queue; //highest priority task.
  }
  else{
    current_task->priority = (current_task->priority+1) % 11;
    current_task = current_task->next;
  }

  //restore priority, when it reaches lowest.
  if(current_task->priority==10)
    current_task->priority = current_task->original_priority;

  eip = current_task->eip;
  esp = current_task->esp;
  ebp = current_task->ebp;

  // Make sure the memory manager knows we've changed page directory.
  current_directory = current_task->page_directory;

  if(sleeping_task)
  {
    remove_from_queue(&ready_queue, sleeping_task);
	  sleeping_task->state = TASK_INTERRUPTIBLE;
    // if(sleeping_task->priority>1)
    //   sleeping_task->priority -= 1;   //increase it's priority before goind to sleep. (avoid starvation.)
    insert_ordered_queue(&wait_queue, sleeping_task);
  }
  else if(task_to_kill)
  {
    remove_from_queue(&ready_queue, task_to_kill);
    free_directory(task_to_kill->page_directory);
    kfree(task_to_kill->ftable);
    kfree(task_to_kill);
  }
  else if(semwait_task)
  {
    sem_t *s = semwait_task->wait_semaphore;
    remove_from_queue(&ready_queue, semwait_task);
    append(&s->queue, semwait_task);  //FIFO order.
    // if(s->rememberd_mutex){
    //   signal(s->rememberd_mutex);
    // }
  }

  if(current_task->state==TASK_RUNNING)
	  perform_task_switch(eip, current_directory->physicalAddr, ebp, esp);
}

// Causes the process to surrender the CPU and go to sleep for n seconds.
//   The function returns 0 if the requested time has elapsed, or the
//   number of seconds left to sleep, if the call was interrupted by a
//          signal handler.
int sleep(unsigned int secs){
  if(current_task->id==1){
	  print("PID: 1 never sleeps.\n");
	  return secs;
  }
  current_task->sleepInterval = tick + secs*frequency;
  current_task->state = TASK_INTERRUPTIBLE_READY;
  switch_task();
}

// Returns the pid of the current process.
int getpid(){
    return current_task->id;
}

// Set the priority of the process. pid is the process id returned by
//   getpid(). newpriority is the new priority value between 1 and 10,
//   where 1 is highest priority. The return value is the resulting
//   priority of the pid. If the pid is invalid then the return value is 0.
int setpriority(int pid, int new_priority){
  asm volatile("cli");
  if(new_priority<1 || new_priority >10){
    asm volatile("sti");
    return 0;
  }

  int priority=0;
  // Add it to the end of the ready queue.
  task_t *iterator = (task_t*)ready_queue;

  while (iterator){
    if(iterator->id == pid){
      if(iterator!=current_task){
        priority = iterator->original_priority;
        asm volatile("sti");
        return priority;
      }
      remove_from_queue(&ready_queue, iterator);
      iterator->original_priority = iterator->priority = new_priority;
      insert_ordered_queue(&ready_queue, iterator);
      asm volatile("sti");
      return new_priority;
    }
    iterator = iterator->next;
  }

  asm volatile("sti");
  return 0;

}

int getpriority(){
    return current_task->priority;
}

u32int *clone_ftable(u32int *ftable){
    u32int *new_ftable = (u32int *)kmalloc(MAX_OPEN_FILES*sizeof(u32int));
    memcpy((void*)new_ftable, (void*)ftable, MAX_OPEN_FILES*sizeof(u32int));
    int i=3;
    for(; i<MAX_OPEN_FILES; i++){
      if(ftable[i]){
        pipe_t *p = find_pipe(&pipelist, i);
        p->refcount++;
      }
    }
    return new_ftable;
}

int fork(){
    // We are modifying kernel structures, and so cannot
    asm volatile("cli");

    // Take a pointer to this process' task struct for later reference.
    task_t *parent_task = (task_t*)current_task;

    // Clone the address space.
    page_directory_t *directory = clone_directory(current_directory);

    // Create a new process.
    task_t *new_task = (task_t*)kmalloc(sizeof(task_t));

    new_task->ftable = clone_ftable(current_task->ftable);
    pipe_t *p = find_pipe(&pipelist, 3);

    new_task->id = next_pid++;
    new_task->esp = new_task->ebp = 0;
    new_task->eip = 0;
    new_task->total_semaphore = 0;
    new_task->page_directory = directory;
    new_task->heap = create_heap(UHEAP_START, UHEAP_START+UHEAP_INITIAL_SIZE, 0xCFFFF000, 0, 0);
    //create_heap( start,  end_addr,  max, u8int supervisor, u8int readonly)

    new_task->priority = parent_task->priority; //inherit priority of parent_task
    new_task->sleepInterval = 0;
    new_task->state = TASK_RUNNING;
    new_task->parent = parent_task->id;         //set the parent ID.
    new_task->next = 0;

    insert_ordered_queue(&ready_queue, new_task);

    // This will be the entry point for the new process.
    u32int eip = read_eip();

    // We could be the parent or the child here - check.
    if (current_task == parent_task){
        // We are the parent, so set up the esp/ebp/eip for our child.
        u32int esp; asm volatile("mov %%esp, %0" : "=r"(esp));
        u32int ebp; asm volatile("mov %%ebp, %0" : "=r"(ebp));
        new_task->esp = esp;
        new_task->ebp = ebp;
        new_task->eip = eip;
        asm volatile("sti");
        return new_task->id;
    }else{
        // We are the child.
        return 0;
    }
}

// Terminates the current process, cleaning up the resources allocated
//   to the process.
void exit(){
    if(current_task->id==1){
      print("Keeping PID 1 alive all the time.\n");
      return ;
    }
    current_task->state = TASK_ZOMBIE;
    int i=3;
    for(; i<MAX_OPEN_FILES; i++){
      if(current_task->ftable[i])
        close_pipe(current_task->ftable[i]);
    }

    switch_task();
}

// Causes the process to surrender the CPU. The result is that the process
//   with the highest priority is assigned the CPU. Note: This may be the
//   current process *if* it is the highest priority.
void yield(){
  current_task->state = TASK_YIELD;
  switch_task();
}

void printQ(task_t *queue){
  print("\nPid:Priority => ");
  task_t *iterator = queue;
  while(iterator){
    print_dec(iterator->id);
    print(":");
    print_dec(iterator->priority);
    print(" =>");
    iterator = iterator->next;
  }
}


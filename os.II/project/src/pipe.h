#ifndef PIPE_H
#define PIPE_H

#define MAXPIPE_SIZE 12
#define INVALID_PIPE -1
#include "common.h"
#include "task.h"
#include "semaphore.h"

typedef struct semaphore sem_t;

typedef struct pipe{
	u32int fd;
	u32int refcount;
	char *buf;
	u32int used_bytes;
	u32int head, tail;
	// int mutex;	  //mutual exclusion to head, tail, buf
	// int full;     //semaphore for pipe full
	// int empty;    //semaphore for pipe empty
	struct pipe *next;
}pipe_t;

// INTERPROCESS COMMUNICATION:
// 
// pipes are first-in-first-out bounded buffers. Elements are read in the same
//   order as they were written. When writes overtake reads, the first unread
//   element will be dropped. Thus, ordering is always preserved.
//  "read" and "write" on pipes are atomic, i.e., they are indivisible, and
//   they are non-blocking. All pipes are of the same size.

#define INVALID_PIPE -1

// Initialize a new pipe and returns a descriptor. It returns INVALID_PIPE
//   when none is available.
int open_pipe();

// Write the first nbyte of bytes from buf into the pipe fildes. The return value is the
//   number of bytes written to the pipe. If the pipe is full, no bytes are written.
//   Only write to the pipe if all nbytes can be written.
unsigned int write(int fildes, const void *buf, unsigned int nbyte);

// Read the first nbyte of bytes from the pipe fildes and store them in buf. The
//   return value is the number of bytes successfully read. If the pipe is 
//   invalid, it returns -1.
unsigned int read(int fildes, void *buf, unsigned int nbyte);

// Close the pipe specified by fildes. It returns INVALID_PIPE if the fildes
//   is not valid.
int close_pipe(int fildes);

#endif

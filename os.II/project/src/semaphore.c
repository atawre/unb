
#include "semaphore.h"
#include "task.h"


// The next available process ID.
u32int next_semid = 1;
sem_t *semaphorelist=0;

sem_t* remove_sem(sem_t **queue, int id){
  //should we disable interrupts here?
  asm volatile("cli");
  if(!*queue)
  	return 0;

  sem_t *iterator=0;

  //removing the first semaphore from the queue
  if((*queue)->id==id){
	iterator = *queue;
    *queue   = (*queue)->next;
    iterator->next = 0;
    return iterator;
  }

  iterator=*queue;

  while(iterator->next && iterator->next->id!=id)
    iterator = iterator->next;

  //ID not found. Never happen unless wrong code is written.
  if(!iterator->next)
  	return 0;

  sem_t *tmp = iterator->next;

  iterator->next = tmp->next;
  tmp->next = 0;

  asm volatile("sti");
  return tmp;
}

sem_t *find_sem(sem_t **queue, int id){
  asm volatile("cli");
	sem_t *iterator=*queue;
	while(iterator){
		if(iterator->id==id)
			break;
		iterator = iterator->next;
	}
  asm volatile("sti");
	return iterator;
}

//just adding to the end. Not worried about ordering based on semaphore ID.
void add_sem(sem_t **queue, sem_t *sem){
  sem_t *iterator;
  asm volatile("cli");

  if(*queue==0){
  	*queue = sem;
  	asm volatile("sti");
  	return;
  }

  iterator = (sem_t *)*queue;
  while(iterator->next)
    iterator = iterator->next;

  iterator->next = sem;
  asm volatile("sti");
}

//  A semaphore must be initialized by open_sem() before it can be used.
//  Processes waiting on a semaphore are resumed on a first-come first-served
//  basis. n is the number of processes that can be granted access to the critical
//  region for this semaphore simultaneously. The return value is a semaphore
//  identifier to be used by signal and wait. 0 indicates open_sem failed.
int open_sem(int n){
	asm volatile("cli");

	sem_t *s = (sem_t *)kmalloc(sizeof(sem_t));
	if(!s){
		print("\nopen_sem: not enough memory.");
		asm volatile("sti");		
		return 0;
	}

	s->id = next_semid++;
	s->queue = 0;
	s->next  = 0;
	s->nprocs = s->maxProcs = n;
	// s->rememberd_mutex = 0;
	add_sem(&semaphorelist, s);
	asm volatile("sti");
	return s->id;
}

// The invoking process is requesting to acquire the semaphore, s. If the
//   internal counter allows, the process will continue executing after acquiring
//   the semaphore. If not, the calling process will block and release the
//   processor to the scheduler. Returns semaphore id on success of acquiring
//   the semaphore, 0 on failure.
int wait(int s){
  asm volatile("cli");

  // 	if(current_task->total_semaphore==MAX_SEMAPHORE){
		// asm volatile("sti");
		// return 0;
  // 	}

	sem_t *sem=find_sem(&semaphorelist, s);
	if(!sem){
		//print("\nwait: semaphore not found.");
		asm volatile("sti");
		return 0;
	}

	sem->nprocs--;			//-ve value will indicate number of waiting processes.
	// current_task->semlist[current_task->total_semaphore] = sem->id;
	// current_task->total_semaphore++;

	if(sem->nprocs < 0){
		current_task->state = TASK_WAITING;
		current_task->wait_semaphore = sem;
		switch_task();
	}

  	asm volatile("sti");
	return sem->id;
}

// The invoking process will release the semaphore, if and only if the process
//   is currently holding the semaphore. If a process is waiting on
//   the semaphore, the process will be granted the semaphore and if appropriate
//   the process will be given control of the processor, i.e. the waking process
//   has a higher scheduling precedence than the current process. The return value
//   is the seamphore id on success, 0 on failure.
int signal(int s){
	asm volatile("cli");

	//Disabling this strict check, since not clear about requirement here.
	// if(current_task->wait_semaphore!=s){
	// 	asm volatile("sti");
	// 	return 0;
	// }

	sem_t *sem=find_sem(&semaphorelist, s);
	if(!sem){
		//print("\nsignal: semaphore not found.");
		asm volatile("sti");
		return 0;
	}

	// int i=0;
	// int nsem = current_task->total_semaphore;
	// for(i=0; i < nsem; i++){
	// 	if( sem->id==current_task->semlist[i] )
	// 		break;
	// }
	//not holding this semaphore.
	// if(i==nsem){
	// 	asm volatile("sti");
	// 	return 0;
	// }
	// current_task->semlist[i] = current_task->semlist[nsem-1];
	// current_task->total_semaphore = nsem - 1;
	
	// if(sem->nprocs >= sem->maxProcs){
	// 	asm volatile("sti");
	// 	return 0;
	// }

	sem->nprocs++;

	//remove first task.
	if(sem->queue){
		//FIFO order, always removing first task from queue.
		task_t *t = sem->queue;
		remove_from_queue(&sem->queue, t);
		t->state  = TASK_RUNNING;
		t->wait_semaphore = 0;
		t->next   = 0;
		insert_ordered_queue(&ready_queue, t);
	}
  	asm volatile("sti");
	return sem->id;
}

// Close the semaphore s and release any associated resources. If s is invalid then
//   return 0, otherwise the semaphore id.
int close_sem(int s){
  asm volatile("cli");

	//sem_t *sem = find_sem(&semaphorelist, s);
	sem_t *sem = remove_sem(&semaphorelist, s);
	if(!sem){
		//print("\nclose: semaphore not found.");
		asm volatile("sti");
		return 0;
	}

	if(sem->queue){
		task_t *iterator = sem->queue;
		while(iterator){
			remove_from_queue(&sem->queue, iterator);
			iterator->state  = TASK_RUNNING;
			insert_ordered_queue(&ready_queue, iterator);
		}
	}

	kfree(sem);
	asm volatile("sti");
	return s;
}

void printSemaphore(){
  print("\nSemaphore list : ");
  sem_t *iterator = semaphorelist;
  while(iterator){
    print("[");
    print_dec(iterator->id);
    print(", ");
    print_dec(iterator->nprocs);
    print(" : ");
	printQ(iterator->queue);
    print("] =>");
    iterator = iterator->next;
  }
  print(" Null\n");
}


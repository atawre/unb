#include "pipe.h"
#include "task.h"


// The next available file FD, 0,1,2 are reserved.
u32int next_fd = 3;
pipe_t *pipelist=0;
extern sem_t *semaphorelist;

pipe_t* remove_pipe(pipe_t **queue, u32int fd){
  //should we disable interrupts here?
  asm volatile("cli");

  pipe_t *iterator=0;

  //removing the first pipe from the queue
  if((*queue)->fd==fd){
	iterator = *queue;
    *queue   = (*queue)->next;
    iterator->next = 0;
    return iterator;
  }

  iterator=*queue;

  while(iterator->next && iterator->next->fd!=fd)
    iterator = iterator->next;

  //ID not found. Never happen unless wrong code is written.
  if(!iterator->next)
  	return 0;

  pipe_t *tmp = iterator->next;

  iterator->next = tmp->next;
  tmp->next = 0;

  asm volatile("sti");
  return tmp;
}

pipe_t *find_pipe(pipe_t **queue, u32int fd){
  asm volatile("cli");
	pipe_t *iterator=*queue;
	while(iterator){
		if(iterator->fd==fd)
			break;
		iterator = iterator->next;
	}
  asm volatile("sti");
	return iterator;
}

//just adding to the end. Not worried about ordering based on semaphore ID.
void add_pipe(pipe_t **queue, pipe_t *pipe){
  pipe_t *iterator;
  asm volatile("cli");

  if(*queue==0){
  	*queue = pipe;
  	asm volatile("sti");
  	return;
  }

  iterator = (pipe_t *)*queue;
  while(iterator->next)
    iterator = iterator->next;

  iterator->next = pipe;
  asm volatile("sti");
}

// Initialize a new pipe and returns a descriptor. It returns INVALID_PIPE
//   when none is available.
int open_pipe(){

	if(next_fd >= MAX_OPEN_FILES){
		print("\nopen_pipe : too many open files.");
		return INVALID_PIPE;
	}

	pipe_t *p = (pipe_t *)kmalloc(sizeof(pipe_t));
	if(!p){
		print("\nopen_pipe: not enough memory.");
		return 0;
	}

	p->buf = (char*)kmalloc(MAXPIPE_SIZE);
	if(!p->buf){
		print("\nopen_pipe: not enough memory.");
		kfree(p);
		return 0;
	}

	current_task->ftable[next_fd] = 1;
	p->fd = next_fd++;
	p->refcount = 1;
	p->used_bytes = 0;
	p->next  = p->head = p->tail = 0;
	// p->mutex = open_sem(1);
	// p->full  = open_sem(0);
	// p->empty = open_sem(0);

	// sem_t *empty = find_sem(&semaphorelist, p->empty);
	// empty->rememberd_mutex = p->mutex;

	// sem_t *full = find_sem(&semaphorelist, p->full);
	// full->rememberd_mutex = p->mutex;

	add_pipe(&pipelist, p);
	return p->fd;
}

// Write the first nbyte of bytes from buf into the pipe fildes. The return value is the
//   number of bytes written to the pipe. If the pipe is full, no bytes are written.
//   Only write to the pipe if all nbytes can be written.
unsigned int write(int fildes, const void *buf, unsigned int nbyte){
	asm volatile("cli");

	if(!current_task->ftable[fildes]){
		print("\nwrite : invalid file descriptor.");
		asm volatile("sti");
		return 0;
	}

	pipe_t *p=find_pipe(&pipelist, fildes);
	if(!p){
		print("\nwrite: pipe not found.");
		asm volatile("sti");
		return INVALID_PIPE;
	}

	// wait(p->mutex);

	if(p->used_bytes==MAXPIPE_SIZE){
		asm volatile("sti");
		// wait(p->full);					//we always release mutex before process is switched.
		return 0;
	}

	u32int free_bytes = MAXPIPE_SIZE - p->used_bytes;
	if(free_bytes < nbyte)
		nbyte = free_bytes;

	u32int first_chunk  = nbyte;
	u32int second_chunk = 0;
	if(p->tail+nbyte > MAXPIPE_SIZE){
		first_chunk  = MAXPIPE_SIZE - p->tail;
		second_chunk = nbyte - first_chunk;
	}

 	p->tail = p->tail % MAXPIPE_SIZE;
	memcpy((void *)(p->buf+p->tail), buf, first_chunk);
	p->tail = p->tail + first_chunk;
 	p->tail = p->tail % MAXPIPE_SIZE;

	if(second_chunk){
		memcpy((void *)(p->buf+p->tail), buf+first_chunk, second_chunk);
		p->tail = p->tail + second_chunk;
 		p->tail = p->tail % MAXPIPE_SIZE;
	}

	p->used_bytes += nbyte;

	// signal(p->mutex);
	// signal(p->empty);
	asm volatile("sti");
	return nbyte;
}

// Read the first nbyte of bytes from the pipe fildes and store them in buf. The
// return value is the number of bytes successfully read. If the pipe is invalid, it returns -1.
unsigned int read(int fildes, void *buf, unsigned int nbyte){
	asm volatile("cli");
	if(!current_task->ftable[fildes]){
		print("\nread : invalid file descriptor.");
		asm volatile("sti");
		return 0;
	}

	pipe_t *p=find_pipe(&pipelist, fildes);
	if(!p){
		print("\nread: pipe not found.");
		asm volatile("sti");		
		return INVALID_PIPE;
	}

	// wait(p->mutex);

	if(!p->used_bytes){
		// wait(p->empty);		//we always release mutex before process is switched.
		asm volatile("sti");
		return 0;
	}

	if(p->used_bytes < nbyte)
		nbyte = p->used_bytes;

	u32int first_chunk  = nbyte;
	u32int second_chunk = 0;
	if(p->head+nbyte > MAXPIPE_SIZE){
		first_chunk  = MAXPIPE_SIZE - p->head;
		second_chunk = nbyte - first_chunk;
	}

	p->head = p->head % MAXPIPE_SIZE;
	memcpy(buf, (void *)(p->buf+p->head), first_chunk);
	p->head = p->head + first_chunk;
	p->head = p->head % MAXPIPE_SIZE;

	if(second_chunk){
		memcpy(buf+first_chunk, (void *)(p->buf+p->head), second_chunk);
		p->head = p->head + second_chunk;
		p->head = p->head % MAXPIPE_SIZE;
	}

	p->used_bytes -= nbyte;

	// signal(p->mutex);
	// signal(p->full);
	asm volatile("sti");
	return nbyte;
}

// Close the pipe specified by fildes. It returns INVALID_PIPE if the fildes
//   is not valid.
int close_pipe(int fildes){
	asm volatile("cli");
	pipe_t *p=find_pipe(&pipelist, fildes);
	if(!p){
		print("\nclose_pipe: pipe not found.");
		asm volatile("sti");
		return INVALID_PIPE;
	}

	// wait(p->mutex);
	p->refcount--;
	current_task->ftable[fildes] = 0;
	if(0==p->refcount){
		p = remove_pipe(&pipelist, fildes);
		if(p)
			kfree(p);
	}else{
		// signal(p->mutex);
	}

	asm volatile("sti");
	return fildes;
}

// monitor.h -- Defines the interface for monitor.h
//              From JamesM's kernel development tutorials.

#ifndef MONITOR_H
#define MONITOR_H

#include "common.h"

// Write a single character out to the screen.
void monitor_put(char c);

// Clear the screen to all black.
void monitor_clear();

// Output a null-terminated ASCII string to the monitor.
void monitor_write(char *c);

// Output a null-terminated ASCII string to the monitor
void print(const char *c);

// Print an unsigned integer to the monitor in base 16
void print_hex(unsigned int n);

// Print an unsigned integer to the monitor in base 10
void print_dec(unsigned int n);

#endif // MONITOR_H

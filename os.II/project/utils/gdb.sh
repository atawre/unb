cd src; make clean; make
cd ..
sudo mount -o loop floppy.img /mnt
sudo cp src/kernel /mnt
sudo umount /mnt
gdb -q -iex 'set architecture i386' -iex 'file src/kernel' -iex 'symbol-file src/kernel'  -iex 'target remote | qemu-system-i386 -gdb stdio -S -kernel src/kernel 2> /dev/null' 

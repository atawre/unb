#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

#define PIPE_BUFFER_SIZE 12

void test_function()
{
    // Ignore process #1, otherwise this test will probably fail weirdly.
    if(fork() != 0){
        for(;;);
    }

    print("Starting test\n");

    int pipe = open_pipe();
    int child_wait_sem = open_sem(0);
    int parent_wait_sem = open_sem(0);
    int ret = fork();

    const int NUMBER_VALUES_READ = 2;
    if(ret == 0){
        // Second: after parent fills up the pipe, the child reads some values from it.
        wait(child_wait_sem);
        print("Child starting first read \n");
        int val = 0xDEADBEEF;
        int k;
        for(k = 0; k < NUMBER_VALUES_READ; k++){
            int retval = read(pipe, &val, sizeof(int));
            print("[Child] Read the following value: "); print_dec(val); print("; return code: "); print_dec(retval); print("\n");
            // Use asserts to check that the read failed after reading the correct number of values.
            if(retval == 0) {
                ASSERT(k == PIPE_BUFFER_SIZE / sizeof(int));
                break;
            }
            ASSERT(val == k);
        }

        signal(parent_wait_sem);

        // Finally: when the parent is done, the child reads all the values from the pipe
        wait(child_wait_sem);
        print("Child starting second read \n");
        int i;
        for(i = 0; ; k++, i++){
            int retval = read(pipe, &val, sizeof(int));
            print("[Child] Read the following value: "); print_dec(val); print("; return code: "); print_dec(retval); print("\n");
            // Use asserts to check that the read failed after reading the correct number of values.
            if(retval == 0) {
                ASSERT(k == PIPE_BUFFER_SIZE / sizeof(int) + NUMBER_VALUES_READ);
                break;
            }
            ASSERT(val == k);
        }

    } else {
        // First: parent writes values to the pipe until it is full
        int j;
        print("Parent starting first write \n");
        for(j = 0; j < 100000; j++){
            int retval = write(pipe, &j, sizeof(int));
            print("[Parent] Write the following value: "); print_dec(j); print("; return code: "); print_dec(retval); print("\n");
            // Use asserts to check that the write failed after writing the correct number of values.
            if(retval == 0){
                ASSERT(j >= PIPE_BUFFER_SIZE / sizeof(int));
                break;
            }
        }

        signal(child_wait_sem);

        // Third: The parent writes some more values to the pipe (the pipe is still partially full from before).
        wait(parent_wait_sem);
        print("Parent starting second write \n");
        int expected_j = j + NUMBER_VALUES_READ;
        for(; j < 100000; j++){
            int retval = write(pipe, &j, sizeof(int));
            print("[Parent] Write the following value: "); print_dec(j); print("; return code: "); print_dec(retval); print("\n");
            // Use asserts to check that the write failed after writing the correct number of values.
            if(retval == 0){
                ASSERT(j >= PIPE_BUFFER_SIZE / sizeof(int));
                ASSERT(j == expected_j);
                break;
            }
        }
        signal(child_wait_sem);
    }

    print("Process #"); print_dec(getpid()); print(" is done\n");



}


#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

#define PAGE_SIZE 4096
void test_function()
{


    void *addr1 = alloc(PAGE_SIZE * 4.5, 0);
    void *addr2 = alloc(PAGE_SIZE * 3.5, 1);
    void *addr3 = alloc(PAGE_SIZE * 1, 0);
    void *addr4 = alloc(PAGE_SIZE * 2, 1);
    void *addr5 = alloc(PAGE_SIZE * 23.5, 0);
    void *addr6 = alloc(PAGE_SIZE * 12, 0);
    void *addr7 = alloc(PAGE_SIZE * 5, 1);
    void *addr8 = alloc(PAGE_SIZE * 2, 0);
    void *addr9 = alloc(PAGE_SIZE * 1, 1);
    void *addr10 = alloc(PAGE_SIZE * 1.5, 0);

    free(addr1);
    free(addr7);
    free(addr3);
    free(addr5);
    free(addr9);

    void *addr11 = alloc(18450, 1);

    free(addr6);
    free(addr4);
    free(addr10);
    free(addr2);
    free(addr8);
    free(addr11);

    void *final = alloc(50 * PAGE_SIZE, 0);

    // Print the addresses. This should return addresses that are in the user heap.
    print_hex(final);
    print("\n");
    print_hex(addr1);
    print("\n");

    // If all blocks were freed correctly, "final" should be allocated at the same address as "addr1"
    // In addition, final and addr1 should be non-null (this process is only requesting ~250K of memory on the heap, 
    // which is more than reasonable - so alloc() shouldn't be failing)
    ASSERT((unsigned int)final == (unsigned int)addr1);
    ASSERT(final != 0);
    ASSERT(addr1 != 0);
}

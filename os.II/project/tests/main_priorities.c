#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

void test_function()
{
    // Set priority of the process to 5 before we start
    print("Returned: "); print_dec(setpriority(getpid(), 5)); print("\n");

    // For this test only: this is actually an intended line that is helpful.
    if(fork() != 0){
        for(;;);
    }

    print("Returned: "); print_dec(setpriority(getpid(), -1)); print("\n");
    print("Returned: "); print_dec(setpriority(getpid(), 1)); print("\n");
    print("Returned: "); print_dec(setpriority(getpid(), 0)); print("\n");
    print("Returned: "); print_dec(setpriority(getpid(), 10)); print("\n");
    print("Returned: "); print_dec(setpriority(getpid(), 11)); print("\n");
    print("Returned: "); print_dec(setpriority(1, 3)); print("\n");
    print("Returned: "); print_dec(setpriority(0, 2)); print("\n");
    print("Returned: "); print_dec(setpriority(4, 2)); print("\n");

    print("Process #"); print_dec(getpid()); print(" is exiting\n");
}

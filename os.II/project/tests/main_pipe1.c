#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

#define PIPE_BUFFER_SIZE 12
void test_function()
{
    if(fork() != 0){
        for(;;);
    }

    print("Starting test\n");
    int pipe = open_pipe();
    int sem = open_sem(0);
    int ret = fork();
    int i;
    
    if(ret == 0){
        wait(sem);
        print("Child has started reading values \n");

        int val = 0xDEADBEEF;
        for(i = 0; i < 4; i++){
            int read_ret = read(pipe, &val, sizeof(int));
            print("Read Returned: "); print_dec(read_ret); print("; Value Read: "); print_dec(val); print("\n");
        }
    } else {
        // Fill the pipe with numbers 1..N
        for(i = 0; i < 3; i++){
            int write_ret = write(pipe, &i, sizeof(int));
            print("Write Returned: "); print_dec(write_ret); print("\n");
        }
        signal(sem);
    }

    print("Process "); print_dec(getpid()); print(" has finished\n");


}


#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

void test_function()
{
    const int MEMSIZE = 10000000;
    unsigned char* a = alloc(MEMSIZE, 0);
    print("Allocated a memory block at: "); print_dec(a); print("\n");
    unsigned int i;
    for(i = 0; i < MEMSIZE; i++){
        a[i] = 0xEC;
    }
    free(a);

    print("Test finished \n");
}


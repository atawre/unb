#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}


void test_function()
{
    print("Running user program \n");

    // Ignore process #1 for testing purposes
    if(fork() != 0){
        for(;;);
    }

    int parent_id = getpid();

    // Create 4 total processes
    fork();
    fork();

    // Change process priorities so that they're all different.
    if(getpid() == 2){
        setpriority(getpid(), 3);
    }
    if(getpid() == 3){
        setpriority(getpid(), 4);
    }
    if(getpid() == 4){
        setpriority(getpid(), 5);
    }
    if(getpid() == 5){
        setpriority(getpid(), 6);
    }

    int count = 0;
    while(1){
        count++;
        // This if statement is to reduce the number of lines written to the console, so it's possible to actually read them
        // without it, we'd be writting over 100 lines/second, which is too fast to read.
        if(count % 20000 == 0 && getpid() != parent_id){
            print("Proc "); print_dec(getpid()); print(" ran \n");
        }
        yield();
    }

    print("Proc #"); print_dec(getpid()); print(" is done running \n");
}

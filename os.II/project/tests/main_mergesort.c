#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

// This merge sort algorithm is implemented from the pseudocode on wikipedia (https://en.wikipedia.org/wiki/Merge_sort)
// it uses the top-down implementation.
//

unsigned int rand(unsigned int *seed)
{
    return (*seed = *seed * 214013L + 2531011L) & 0x7ffffff;
}

int comparator_int(void *a,void *b)
{
    return (a < b) ? -1 : (a == b) ? 0 : 1;
}

void copy_array(void **src, void **dest, unsigned int size)
{
    unsigned int i;
    for(i = 0; i < size; i++) {
        dest[i] = src[i];
    }
}

void merge_combine(void **a, unsigned int begin, unsigned int middle, unsigned int end, void **b, int (*comparator)(void *a, void *b))
{
    unsigned int i = begin;
    unsigned int j = middle;
    unsigned int k;
    for(k = begin; k < end; k++){
        if(i < middle && (j >= end || comparator(a[i], a[j]) == -1)){
            b[k] = a[i];
            i++;
        } else {
            b[k] = a[j];
            j++;
        }
    }
}

void merge_split(void **b, unsigned int begin, unsigned int end, void **a, int (*comparator)(void *a, void *b))
{
    if(end - begin < 2){
        return;
    }

    unsigned int middle = (end + begin) / 2;
    merge_split(a, begin, middle, b, comparator);
    merge_split(a, middle, end, b, comparator);
    merge_combine(b, begin, middle, end, a, comparator);
}

void merge_sort(void **items, unsigned int size, int (*comparator)(void *a, void *b))
{
    void **b = alloc(sizeof(void*) * size,0);
    copy_array(items, b, size);
    merge_split(b, 0, size, items, comparator);
}

void test_function()
{
    // Ignore process #1
    if(fork() != 0){
        for(;;);
    }

    print("Starting test \n");
    // Create 8 total processes
    fork();
    fork();
    fork();

    // Generate some random numbers
    unsigned int seed = (unsigned int)getpid();
    int len = 200000;
    unsigned int *nums = alloc(sizeof(unsigned int) * len, 0);
    int i;
    for(i = len -1; i >= 0; i--){
        nums[i] = rand(&seed);
    }

    // Force at least 1 context switch, to make things a bit more interesting
    yield();

    // Sort
    void *v = nums;
    merge_sort(v, len, comparator_int);

    // One way to validate the result is to check that the numbers are sorted using assertions, since we can't print
    // an array this large to screen. ie check that a[i] <= a[i+1] for all a[i]
    for(i = 0; i < len - 1; i++){
        ASSERT(nums[i] <= nums[i + 1]);
    }

    print("Process #"); print_dec(getpid()); print(" is exiting. \n");
    print(" -> Smallest Value: "); print_dec(nums[0]); print("\n");
    print(" -> Largest Value: "); print_dec(nums[len -1]); print("\n");
}


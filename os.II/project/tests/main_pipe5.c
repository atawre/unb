#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

struct rational_number
{
    int numerator;
    int denominator;
};

void print_rational(struct rational_number *rational)
{
    print_dec(rational->numerator); print("/"); print_dec(rational->denominator);
}

void test_function()
{
     int sem = open_sem(0);
    int pipe1 = open_pipe();
    int pipe2 = open_pipe();

    if(fork() == 0){
        // Child must wait
        wait(sem);

        struct rational_number value;
        int ret = read(pipe1, &value, sizeof(struct rational_number));
        print("[Child] read value: "); print_rational(&value); print(" with return code: "); print_dec(ret); print("\n");

        ret = read(pipe1, &value, sizeof(struct rational_number));
        print("[Child] read value: "); print_rational(&value); print(" with return code: "); print_dec(ret); print("\n");

        ret = read(pipe2, &value, sizeof(struct rational_number));
        print("[Child] read value: "); print_rational(&value); print(" with return code: "); print_dec(ret); print("\n");

        ret = read(pipe2, &value, sizeof(struct rational_number));
        print("[Child] read value: "); print_rational(&value); print(" with return code: "); print_dec(ret); print("\n");

    } else {
        // Parent goes first

        struct rational_number value1 = {.numerator = 512, .denominator = 62};
        struct rational_number value2 = {.numerator = 999999, .denominator = 65};
        struct rational_number value3 = {.numerator = 2304982, .denominator = 908437};
        struct rational_number value4 = {.numerator = 8974215, .denominator = 26532};

        write(pipe1, &value1, sizeof(struct rational_number));
        write(pipe1, &value2, sizeof(struct rational_number));
        write(pipe2, &value3, sizeof(struct rational_number));
        write(pipe2, &value4, sizeof(struct rational_number));

        signal(sem);
    }

    print("Process #"); print_dec(getpid()); print(" is exiting\n");

}
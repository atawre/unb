#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

#define PIPE_BUFFER_SIZE 12

void test_function()
{
   int pipe = open_pipe();
    int sem = open_sem(0);
    print("Pipe id: "); print_dec(pipe); print("\n");
    print("Sem id: "); print_dec(sem); print("\n");

    int ret = fork();
    if(ret == 0){
        // Make sure we run second
        int sem_ret = wait(sem);
        // Sem was closed - not a normal wakeup. Rather, this is an error
        print("Child return code for wait(): "); print_dec(sem_ret); print("\n");

        // Should also error. Pipe is closed.
        int tmp;
        int pipe_ret = read(pipe, &tmp, sizeof(int));
        print("Child return code for read(): "); print_dec(pipe_ret); print("\n");

    } else {
	     int i;
        for(i = 0; i < (PIPE_BUFFER_SIZE / sizeof(int)); i++){
            write(pipe, &i, sizeof(int));
        }
        signal(sem);
    }

    // Test sem return codes - one should be an error
    int close_ret = close_sem(sem);
    if(ret == 0) {
        print("Child return code for sem close(): "); print_dec(close_ret); print("\n");
    } else {
        print("Parent return code for sem close(): "); print_dec(close_ret); print("\n");
    }

    // Test pipe return codes - one should be an error
    int pipe_ret = close_pipe(pipe);
    if(ret == 0) {
        print("Child return code for pipe close(): "); print_dec(pipe_ret); print("\n");
    } else {
        print("Parent return code for pipe close(): "); print_dec(pipe_ret); print("\n");
    }

}

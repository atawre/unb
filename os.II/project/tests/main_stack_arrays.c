#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

void test_function()
{

    int arr[10];
    int ret = fork();
    int i;

    // Fill parent array with 0->9, fill child array with 9->0
    if(ret == 0){
        for(i = 0; i < 10; i++){
            arr[i] = 9-i;
        }
    } else {
        for(i = 0; i < 10; i++){
            arr[i] = i;
        }
    }
    yield();

    // Print array contents
    print("Process #"); print_dec(getpid()); print(" has array: ");
    for(i = 0; i < 10; i++){
        print_dec(arr[i]); print(" ");
    }
    print("\n");
    yield();

    // Overwrite a couple of values
    if(ret == 0){
        arr[7] = 42;
    } else {
        arr[2] = 9001;
    }
    yield();

    // Print arrays again
    print("Process #"); print_dec(getpid()); print(" has array: ");
    for(i = 0; i < 10; i++){
        print_dec(arr[i]); print(" ");
    }
    print("\n");

}


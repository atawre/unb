#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "multiboot.h"
#include "task.h"
#include "test.h"
#include "semaphore.h"

// I'm moving my test code to another function so I don't accidently delete anything vital in main()
void test_function();

u32int initial_esp;
int main(struct multiboot *mboot_ptr, u32int initial_stack)
{
    initial_esp = initial_stack;
    // Initialise all the ISRs and segmentation
    init_descriptor_tables();
    // Initialise the screen (by clearing it)
    monitor_clear();
    // Initialise the PIT to 100Hz
    asm volatile("sti");
    init_timer(25);
    // Start paging.
    initialise_paging();

    // Start multitasking.
    initialise_tasking();

    test_function();

    return 0;
}

void test_function()
{
    if(fork() != 0){
        for(;;);
    }

    int parent_sem = open_sem(1);
    int child_sem = open_sem(0);
    int ret = fork();

    // The two processes take turns running, by setting the other's semaphore.
    // The first process to run is the parent, then the child will get to run, etc
    // After taking turns roughly 5 times, the parent closes the child_sem - from this point on, any system calls involving
    //  that semaphore should return 0 [error code]
    if(ret == 0){
        // Child
        int i;
        for(i = 0; i < 10; i++){
            int wait_retval = wait(child_sem);
            print("[Child Process] Child Semaphore ID: "); print_dec(child_sem); print("; Wait returned: "); print_dec(wait_retval); print("\n");
            signal(parent_sem);
        }
        int close_retval = close_sem(child_sem);
        print("[Child Process] Close returned: "); print_dec(close_retval); print("\n");
    } else {
        // Parent
        int i;
        for(i = 0; i < 5 ; i++){
            wait(parent_sem);
            int signal_retval = signal(child_sem);
            print("[Parent Process] Child Semaphore ID: "); print_dec(child_sem); print("; Signal returned: "); print_dec(signal_retval); print("\n");
        }
        int sem_close = close_sem(child_sem);
        print("[Parent Process] Close returned:"); print_dec(sem_close); print("\n");
    }
}


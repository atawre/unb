#ifndef PAGING
#define PAGING
#include <stdlib.h>
#include <string.h>
#include <assert.h>


unsigned mask(unsigned x, unsigned y){
   unsigned m = 0;
   for (unsigned i=x; i<=y; i++)
       m |= 1 << i;
   return m;
}

#define frame(x)    (x&mask(0,10))         //trailing 11 bits
#define table(x)    ((x&mask(11,20))>>11)  //middle 10 bits
#define pdir(x)     ((x&mask(21,31))>>21)   //leading 11 bits
#define swapkey(x)  ((x&mask(11,31))>>11)   //leading 21 bits
#define PAGESIZE  2048

typedef struct page
{
   unsigned present    ;   // Page present in memory
   unsigned rw         ;   // Read-only if clear, readwrite if set
   unsigned user       ;   // Supervisor level only if clear
   unsigned accessed   ;   // Has the page been accessed since last refresh?
   unsigned dirty      ;   // Has the page been written to since last refresh?
   unsigned swapped    ;   // Has the page been written to since last refresh?
   unsigned anon       ;   // Anon pages are not file backed.
   unsigned frame      ;  // Frame address (shifted right 12 bits)
   unsigned *memory;
   unsigned swapkey;
} page_t;


void *getpage(){
  void *tmp = malloc(PAGESIZE);
  memset(tmp, 0, PAGESIZE);
  return tmp;
}

typedef struct page_table
{
   page_t pages[1024];
} ptable_t;

typedef struct page_directory
{
   ptable_t *tables[2048]; //Array of pointers to pagetables.
} pdir_t;

typedef struct page_directory *pdir_p;
typedef struct page_table *ptable_p;
typedef struct page *page_p;

typedef struct frame{
  int free:1;
  page_p page;
}frame_t;

typedef struct frame *frame_p;

pdir_t pdir;

void init_pdir(){
    for(int i=0; i<2048; i++)
      pdir.tables[i] = 0;
}

extern int uframes;
extern frame_p frames;
//extern unsigned *frames;

extern unsigned next_frame;
//second chance algorithm.
int getframe(){
    int count=uframes;
    int i=next_frame;
    while(count>0){
        if(frames[i].free){
            next_frame=(i+1)%uframes;
            return i;
        }else if(frames[i].page->accessed){
            frames[i].page->accessed = 0;
        }
        i = (i+1)%uframes;
        count--;
    }

    count=uframes;
    i=next_frame;
    while(count>0){
        if(!frames[i].page->dirty){
            next_frame=(i+1)%uframes;
            return i;
        }
        i = (i+1)%uframes;
        count--;
    }

    i=next_frame;
    next_frame=(next_frame+1)%uframes;
    assert(i!=uframes);
    assert(next_frame!=uframes);
    assert(i!=-1);
    return i;
}




int setframe(int i, page_p page){
  if(!page->user){
    return -1;
  }
  int old = frames[i].free;
  frames[i].free = 0;
  page->frame    = i;     //cycle.
  frames[i].page = page;
  return old;
}

int unsetframe(int i){
  int old = frames[i].free;
  frames[i].free = 1;
  //frames[i].page->frame = -1; //cycle.
  return old;
}

void init_frames(){
  frames = (frame_p)malloc(sizeof(frame_t)*uframes);
  for(int i=0; i<uframes; i++){
    frames[i].free = 1;
    frames[i].page = 0;
  }
}

int get_live_page_count(){
  int count=0;
  for(int i=0; i<uframes; i++){
    if(!frames[i].free)
      count++;
  }
  return count;
}

#endif

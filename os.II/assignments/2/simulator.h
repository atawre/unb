#ifndef SWAP_SIM_H
#define SWAP_SIM_H
#include "paging.h"
#include "swap.h"
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/mman.h>
#include <assert.h>

#define READONLY  0
#define SHARED    1
#define ANON      2
#define ONE_GB 1073741824

typedef struct vma{
    char *fname;
    int fd;
    unsigned mapAddr;
    unsigned endAddr;
    int type;
    uint32_t* mmap;
    struct vma *next;
}*vma_p;

extern vma_p vlist;

typedef struct cmd{
    char c;
    unsigned addr;
    unsigned data;
    vma_p varea;
}*cmd_p;

void print_cmd(cmd_p cmd){
    if(cmd->c=='r')
      printf("(r %u => %u) ", cmd->addr, cmd->data);
    else
      printf("(w %u %u) ", cmd->addr, cmd->data);
    printf("\n");
    //printf("tbl : %d page: %d\n", pdir(cmd->addr), table(cmd->addr));
}

vma_p remove_from_queue(vma_p *vlist, vma_p f){
  vma_p iterator=*vlist;

  while(iterator->next && iterator->next!=f)
    iterator = iterator->next;

  //removing the first process from the queue
  if(iterator==f)
    *vlist = f->next;

  iterator->next = f->next;
  f->next = 0;
  free(f->fname);
  free(f);
  return iterator->next;
}

void append(vma_p vqueue, vma_p v){
    vma_p iterator=vqueue;
    while(iterator->next)
        iterator = iterator->next;
    iterator->next = v;
    v->next = 0;
}

void insert_ordered_queue(vma_p *vqueue, vma_p v){
  vma_p tmp, prev, iterator;
  iterator = (vma_p)*vqueue;
  prev = 0;
  while(iterator){
    if(iterator->mapAddr > v->mapAddr)
      break;
    prev = iterator;
    iterator = iterator->next;
  }

  tmp = 0;
  if(prev){
    tmp = prev->next;
    prev->next = v;
  }else{
    tmp = *vqueue;
    *vqueue = v;
  }
  v->next = tmp;
}

int isMapped(vma_p list, unsigned addr){
    vma_p iterator=list;
    while(iterator){
        if(iterator->mapAddr==addr)
            return 1;
        iterator = iterator->next;
    }
    return 0;
}

vma_p find_vma(vma_p list, unsigned addr){
    vma_p prev=0, iterator=list;
    while(iterator){
      if(iterator->mapAddr <= addr && addr < iterator->endAddr)
        break;
      prev  = iterator;
      iterator = iterator->next;
    }

    if(!iterator){
      printf("VMA not found => %u", addr);
    }
    return iterator;
}

long getFileSize(char *file){
    struct stat sb;
    if (stat(file, &sb) == -1){
        perror("stat");
        exit(1);
    }
    // printf("=> %d\n", sb.st_size);
    return(sb.st_size);
}

extern pdir_t pdir;

vma_p getVMA(char *name, long mapAddr, int type){
    vma_p v     = (vma_p)malloc(sizeof(struct vma));
    int size    = 0;
    int offset  = mapAddr%2048;

    v->mapAddr  = (mapAddr - offset);
    v->type     = type;
    if(type!=ANON)            //ANON pages are not file backed.
      size      = getFileSize(name);
    v->endAddr  = mapAddr + size;
    offset      = v->endAddr%2048;
    v->endAddr += (2048-offset);

    v->fname  = (char *)malloc(strlen(name)+1);
    strcpy(v->fname, name);
    if(strcmp(v->fname, "anon")!=0){
      v->fd     = open(v->fname, O_RDONLY);
      if(v->fd < 0){
        perror("open");
        exit(1);
      }
    }

    return v;
}

void print_vma(vma_p list){
  vma_p iterator=list;
  while(iterator){
    printf("(%u - %u) %s\n", iterator->mapAddr, iterator->endAddr, iterator->fname);
    iterator = iterator->next;
  }
}

//fill out unmapped addresses with anon mappings in VMA list.
void alloc_anon_maps(vma_p *list){
    vma_p prev=0, iterator, tmp;
    unsigned address = 0, maxAddr = 4294967295;

    //add initial node.
    if((*list)->mapAddr > 0){
      tmp = getVMA("anon", 0, ANON);
      tmp->endAddr = (*list)->mapAddr;
      tmp->next = *list;
      *list = tmp;
    }

    iterator=*list;

    while(iterator->next){
      if(iterator->endAddr != iterator->next->mapAddr){
        tmp = getVMA("anon", iterator->endAddr, ANON);
        tmp->endAddr = iterator->next->mapAddr;
        tmp->next = iterator->next;
        iterator->next = tmp;
        iterator = iterator->next; //double increment, as we filled hole.
      }
      iterator = iterator->next;
    }

    //add end node.
    if(iterator->endAddr!=maxAddr){
      tmp = getVMA("anon", iterator->endAddr, ANON);
      tmp->endAddr = maxAddr;
      iterator->next = tmp;
      tmp->next = 0;
    }
}

void get_cmd(cmd_p *cmd, char *line){
    if(!*cmd)
        *cmd    = (cmd_p)malloc(sizeof(struct cmd));
    line[strlen(line) - 1] = '\0'; // eat the newline fgets() stores
    char *token = strtok(line, " ");
    (*cmd)->c   = token[0];
    token = strtok(NULL, " ");
    (*cmd)->addr = (unsigned)atoi(token);
    if((*cmd)->c=='w'){
        token = strtok(NULL, " ");
        (*cmd)->data = (unsigned)atoi(token);
    }else{
        (*cmd)->data = 0;
    }
    (*cmd)->varea = find_vma(vlist, (*cmd)->addr);
}


//read the file backed page (READONLY)
void filein(cmd_p cmd, page_p page){

  if(-1==page->frame){
    printf("Make sure frame is allocated before reading file.\n");
    return;   
  }

  if(!page->memory)
    page->memory = getpage();

  int fd        = cmd->varea->fd;
  off_t offset  = ((cmd->addr - cmd->varea->mapAddr)/PAGESIZE)*PAGESIZE;
  lseek(fd, offset, SEEK_SET);

  if(-1==read(fd, page->memory, PAGESIZE)){
    perror("Read: ");
    printf("Error reading a page from swap.\n");
    exit(1);
  }
  page->present = 1;
}

extern int kframe;
extern unsigned pagefaults;
extern unsigned pagesinswap;
extern unsigned livepages;

void do_cmd(cmd_p cmd){
  if(!cmd->varea)
    return;
  int fault=0;
  vma_p varea = cmd->varea;

  if(varea->type==READONLY && cmd->c=='w'){
    printf("INVALID =>"); print_cmd(cmd);
    return;
  }

  page_p  currentPage;
  int table   = pdir(cmd->addr);
  int page    = table(cmd->addr);
  //int off   = frame(cmd->addr);

  int frame = -1;
  if(!pdir.tables[table]){ //page fault
    pdir.tables[table] = (ptable_p)malloc(sizeof(ptable_t));
    for(int i=0; i<1024; i++) {
        pdir.tables[table]->pages[i].present = 0;
        pdir.tables[table]->pages[i].accessed = 0;
        pdir.tables[table]->pages[i].dirty = 0;
        pdir.tables[table]->pages[i].swapped = 0;
        pdir.tables[table]->pages[i].memory = 0;
    }
  }

  currentPage = &pdir.tables[table]->pages[page];
  currentPage->swapkey  = swapkey(cmd->addr);

  if(!currentPage->present) {
    pagefaults++;
    if (varea->type == ANON) {
      currentPage->anon = 1;
      currentPage->rw = 1;
    } else {
      currentPage->anon = 0;
      currentPage->rw = varea->type;
    }

    //allocate frame and swapout if needed.
    if(cmd->addr < ONE_GB){ 
      currentPage->user = 0;  //kernel 
      frame = kframe++;
    }else {
        currentPage->user = 1;  //user.
        frame = getframe();
        if (!frames[frame].free)
            swapout(frame);
    }
    setframe(frame, currentPage);
  }

  //this can be anon or shared.
  if(currentPage->swapped)
    swapin(cmd->addr, currentPage);

  //it is file backed. ANON pages are not file backed.
  //since earlier swapin attempt didn't mark it present, it's in file and readonly.
  if(!currentPage->present){
    if(varea->type==ANON){
      currentPage->memory   = (unsigned *)getpage();
      currentPage->present  = 1;
    }else{
      filein(cmd, currentPage);
    }
  }

  //offset of byte within a page.
  int offset = cmd->addr % PAGESIZE;
  if(!currentPage->present || !currentPage->memory){
    printf("Anamoly, page should have been present.\n");
    print_cmd(cmd);
    return;
  }

  if(cmd->c=='r'){          //READ command
    if(varea->type==ANON && !currentPage->dirty)
        cmd->data = 0;      //return ZERO, without any allocation.
    else
        cmd->data = ((unsigned *)((char*)currentPage->memory + offset))[0];
  }
  else if(cmd->c=='w')          //WRITE command
  {
      currentPage->dirty = 1;
      ((unsigned *)((char*)currentPage->memory + offset))[0] = cmd->data;
  }

  printf("%u\n", cmd->data);
}

#endif


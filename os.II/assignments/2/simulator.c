#include"paging.h"
#include"simulator.h"
#include"swap.h"
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>

//swap_sim –l my_exe 1073741824 –l my_kernel 0 –s share_space 1500000000 –f 1024 –r mem_trace
void usage(){
    printf("swap_sim –l <file> <map address> -s <cow file>  <map address> -f <frame count> -r <memTrace>\n");
    exit(1);
}

//max logicalAddr 4294967296 = 2^32
char *traceFile = "mem_trace";
char *fname     = "my_exe";
long mapAddr    = 1073741824;
char *sname     = "share_space";
long smapAddr   = 1500000000;
char *kname     = "my_kernel";
long kmapAddr   = 0;
int uframes     = 1024;
int kframe      = 0;    //monotonically increasing kernel frame count.
frame_p frames;
extern pdir_t pdir;
vma_p vlist=0;

unsigned pagefaults=0;
unsigned swapouts=0;
unsigned swapins=0;
unsigned pagesinswap=0;
unsigned livepages=0;
unsigned next_frame=0;

void processArgs(int argc, char *argv[]){
int i = 1;
unsigned addr=0;
    while(i < argc){
        if(strcmp(argv[i], "-l")==0){
            vma_p v = getVMA(argv[i+1], (unsigned)atoi(argv[i+2]), READONLY);
            if(isMapped(vlist, v->mapAddr)){
                printf("Not handling 2 files mapped to the same page.\n");
                exit(1);
            }
            insert_ordered_queue(&vlist, v);
            i += 3;
        }else if(strcmp(argv[i], "-s")==0){
            vma_p v = getVMA(argv[i+1], (unsigned)atoi(argv[i+2]), SHARED);
            if(isMapped(vlist, v->mapAddr)){
                printf("Not handling 2 files mapped to the same page.\n");
                exit(1);
            }
            insert_ordered_queue(&vlist, v);
            i += 3;
        }else if(strcmp(argv[i], "-f")==0){
            uframes = atoi(argv[i+1]);
            i += 2;
        }else if(strcmp(argv[i], "-r")==0){
            traceFile = malloc(strlen(argv[i+1]+1));
            strcpy(traceFile, argv[i+1]);
            i += 2;
        }else{
            printf("Usage: %s -l my_exe 1073741824 -l my_kernel 0 -s share_space 1500000000 -f 1024 -r mem_trace\n", argv[0]);
            break;
        }
    }
}

void print_summary(){
    printf("---------------------------------------------------\n");
    printf("Number of page faults: %d \n", pagefaults);
    printf("Number of pages swapped out: %d \n", swapouts);
    printf("Number of pages swapped in: %d \n", swapins);
    printf("Number of pages in the swap file: %d \n", total_swapped_pages());
    printf("Number of virtual memory pages loaded: %d \n", get_live_page_count());
    printf("---------------------------------------------------\n");
}

int main(int argc, char *argv[]){
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    cmd_p cmd=0;

    processArgs(argc, argv);
    if(!vlist){
        printf("Usage: %s -l my_exe 1073741824 -l my_kernel 0 -s share_space 1500000000 -f 1024 -r mem_trace\n", argv[0]);
        exit(1);
    }

    alloc_anon_maps(&vlist);
    print_vma(vlist);
    getchar();
    init_pdir();
    init_frames();
    create_swap();
    
    fp = fopen(traceFile, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    int n=0;
    vma_p varea;

    //while ((read = getline(&line, &len, stdin)) != -1) {
    while ((read = getline(&line, &len, fp)) != -1) {
        get_cmd(&cmd, line);
        if(!cmd->varea){
          printf("No mappings available for address %u\n", cmd->addr);
          continue;
        }
        do_cmd(cmd);
    }

    free(cmd);
    free(line);
    print_summary();
    return 0;
}

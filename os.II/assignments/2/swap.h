#ifndef SWAP
#define SWAP
#include "paging.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

typedef struct vma *vma_p;
extern vma_p vlist;
extern vma_p find_vma(vma_p, unsigned);

typedef struct swap{
	unsigned key;
	off_t offset;
	struct swap *next;
}swap_t;

typedef struct swap *swap_p;

char *swapFile = "swap";
int swapFd = 0;
swap_p swaplist = 0;
extern unsigned swapouts;
extern unsigned swapins;

int create_swap(){
	swapFd = open(swapFile, O_RDWR|O_TRUNC);
	if(swapFd < 0){
		printf("Failed to open swapfile.\n");
		exit(1);
	}
	return swapFd;
}

void print_swaplist(){
  printf("\nSwap List : ");
  swap_p iterator = swaplist;
  while(iterator){
    printf("(%u - %ld)=> ", iterator->key, iterator->offset);
    iterator = iterator->next;
  }
  printf("\n");
}

int total_swapped_pages(){
  int count=0;
  swap_p iterator = swaplist;
  while(iterator){
    iterator = iterator->next;
    count++;
  }
  return count;
}

swap_p getSwapNode(unsigned key, off_t offset){
	swap_p tmp	= (swap_p)malloc(sizeof(struct swap));
    assert(tmp);
	tmp->next	= 0;
	tmp->key	= key;
	tmp->offset	= offset;
	return tmp;
}

swap_p remove_from_swaplist(unsigned key){
	swap_p iterator = swaplist;
	swap_p prev		= 0;
	while(iterator){
		if(iterator->key==key)
			break;
		prev	 = iterator;
		iterator = iterator->next;
	}

    if(!iterator)
        return NULL;

    //first node.
	if(iterator==swaplist){
		swaplist = iterator->next;
		iterator->next = 0;
		return iterator;
	}

    if(prev)
        prev->next = iterator->next;

	iterator->next = 0;
	return iterator;
}

void add_to_swaplist(swap_p newpage){
    newpage->next = swaplist;
    swaplist = newpage;
}

void swapout(int frame){
	page_p page;
	page = frames[frame].page;	//this is always user page.

    //printf("swapping out a page.\n");

	if(!page->user){	//don't swapout kernel pages.
		//printf("Don't swap out kernel pages.\n");
		return;
	}

    //Read Only pages can be freed without writing to swap.
	if(page->rw || page->anon){
		unsigned key	= page->swapkey;
		off_t offset	= lseek(swapFd, 0, SEEK_END);
		swap_p newpage	= getSwapNode(key, offset);
		add_to_swaplist(newpage);

		if(-1==write(swapFd, page->memory, PAGESIZE)){
			perror("write");
			exit(1);
		}
        page->swapped	= 1;
    }

	if(page->memory)
		free(page->memory);
	page->memory 	= 0;

	//file backed read only pages are not marked swapped.
	//if(!page->rw || !page->anon)
	page->present	= 0;

	unsetframe(page->frame); //mark the frame free.
	swapouts++;
	//return page->frame;
}

void swapin(unsigned address, page_p page){
	if(!page->user){	//can't swapin kernel pages.
		printf("Can't swap in kernel pages.\n");
		return;
	}

	if(-1==page->frame){
		printf("Make sure frame is allocated before swap in.\n");
		return;
	}

    //printf("swapping in a page.\n");

	unsigned key		= swapkey(address);
    swap_p page_on_disk = 0;
    page_on_disk = remove_from_swaplist(key);
    assert(page_on_disk);
	page->swapped		= 0;
	page->present		= 1;

	if(!page->memory)
		page->memory = getpage();

    off_t offset = page_on_disk->offset;
	lseek(swapFd, offset, SEEK_SET);

	if(-1==read(swapFd, page->memory, PAGESIZE)){
		perror("Read: ");
		printf("Error reading a page from swap.\n");
		exit(1);
	}

	free(page_on_disk);
	swapins++;
}

#endif

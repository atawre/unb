
extern char algorithm;
extern int heapSize;
typedef struct fnode{
    int addr;
    int size;
    struct fnode *next;
}*freeNode;

void printFreeList(header bucket){
    freeNode tmp = bucket->freeList->next;
    printf("T%d : ", bucket->id);
    while(tmp){
        printf("(%d, %d) ->", tmp->addr, tmp->size);
        tmp = tmp->next;
    }
    printf("\n");
}

void freeHeap(header bucket){
    freeNode tmp, next;
    tmp = bucket->freeList->next;
    while(tmp){
        next = tmp->next;
        free(tmp);
        tmp = next;
    }
    bucket->freeList->next = NULL;
}

freeNode getFreeNode(int addr, int size){
    freeNode tmp = (struct fnode*)malloc(sizeof(struct fnode));
    tmp->addr = addr;
    tmp->size = size;
    tmp->next = NULL;
    return tmp;
}

int bestfit(header bucket, int size){
    int addr=-1;
    int smallSize=heapSize+1;
    int lowerBound=size;
    freeNode tmp, prev, smallBlock;
    tmp = bucket->freeList;
    smallBlock = NULL;
    while(tmp->next){
        if(tmp->next->size >= lowerBound && tmp->next->size < smallSize){ //find smallest large enough for alloc.
                smallSize = tmp->next->size;
                smallBlock = tmp->next;
                prev = tmp;
        }
        tmp = tmp->next;
    }
    if(smallBlock){ //found.
        if(size==smallBlock->size){ //exact match.
            addr = smallBlock->addr;
            prev->next = smallBlock->next;
            free(smallBlock);
        }else{
            addr = smallBlock->addr;
            smallBlock->addr += size;
            smallBlock->size -= size;
        }
    }else{
        printf("Allocation failed.\n");
    }

    return addr;  //not enough memory.
}

int worstfit(header bucket, int size){
    int addr=-1;
    int bigSize=0;
    int baseFound=0;
    freeNode tmp, prev, bigBlock;
    tmp = bucket->freeList;
    bigBlock = NULL;
    while(tmp->next){
        if(baseFound && bigSize < tmp->next->size){ //find biggest.
                bigSize = tmp->next->size;
                bigBlock = tmp->next;
                prev = tmp;
        }else if(!baseFound && size <= tmp->next->size){ //should not miss exact match.
                bigSize = tmp->next->size;
                bigBlock = tmp->next;
                prev = tmp;
                baseFound=1;
        }
        tmp = tmp->next;
    }
    if(bigBlock->size==size){ //worst bcame best.
        prev->next = bigBlock->next;
        addr = bigBlock->addr;
        free(bigBlock);
    }else if(bigBlock->size > size){ //take space from biggest block.
        addr = bigBlock->addr;
        bigBlock->size -= size;
        bigBlock->addr += size;
    }else{
        printf("Allocation failed.\n");
    }

    return addr;  //not enough memory.
}

int firstfit(header bucket, int size){
    int addr=-1;
    freeNode tmp, prev;
    prev = bucket->freeList;
    tmp = prev->next;
    while(tmp){
        if(tmp->size==size){ //exact match, delete current node.
            addr = tmp->addr;
            prev->next = tmp->next;
            free(tmp);
            return addr;
        }else if(tmp->size > size){ //reduce size of this free node.
            addr = tmp->addr;
            tmp->size -= size;
            tmp->addr += size;
            return addr;
        }
        prev = tmp;
        tmp = tmp->next;
    }
    return -1;  //not enough memory.
}

int new(header bucket, int size){
    if(algorithm=='f')
        return firstfit(bucket, size);
    if(algorithm=='b')
        return firstfit(bucket, size);
    if(algorithm=='w')
        return worstfit(bucket, size);
}

int delete(header bucket, int addr, int size){
    freeNode tmp, prev;
    prev = bucket->freeList;
    tmp = prev->next;

    if(tmp==NULL){//freeList is empty.
        bucket->freeList->next = getFreeNode(addr, size);
        return 1;   //SUCCESS
    }

    if(addr+size > heapSize)   //Invalid request.
        return 1;

    while(tmp){
        if(prev->addr+prev->size==addr){ //at boundary of previous block.
            if(addr+size==tmp->addr){    //merge 2 consecutive free nodes.
                prev->size = prev->size + size + tmp->size;
                prev->next = tmp->next;
                free(tmp);
                return 1;   //SUCCESS
            }else if(addr+size < tmp->addr){  //Suffix for prev
                prev->size += size;
                return 1;   //SUCCESS
            }else{
                printf("Invalid request.\n");
                break;
            }
        }else if(addr < tmp->addr){
            if(addr+size==tmp->addr){   //Prefix for tmp
                tmp->addr = addr;
                tmp->size += size;
                return 1;   //SUCCESS
            }else if(addr+size < tmp->addr){    //new node
                freeNode neo = getFreeNode(addr, size);
                prev->next = neo;
                neo->next = tmp;
                return 1;   //SUCCESS
            }else{
                printf("Invalid request.\n");
                break;
            }
        }else if(tmp->addr==addr){          //double free.
            printf("Thread %d doing double free?\n", bucket->id);
            break;
        }else if(tmp->next==NULL){          //ur @ last node.
            if(tmp->addr+tmp->size==addr){  //Suffix of tmp.
                tmp->size += size;
                return 1;   //SUCCESS
            }else if(tmp->addr+tmp->size < addr && addr+size <= heapSize){
                freeNode neo = getFreeNode(addr, size);
                tmp->next = neo;
                return 1;   //SUCCESS
            }else{
                printf("Invalid request.\n");
                break;
            }
        }
        prev = tmp;
        tmp = tmp->next;
    }
    return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include "cmd_header.h"
#include "freelist.h"
 
#define bufSize 100
#define ALLOCATE -1
#define FREE -2

struct Header headers[26];
struct Header h;
int heapSize;
char algorithm;
int debug=0;
/*
 Output generated includes the following:
    • If an allocation fails:
        o Thread a failed to allocate x memory on line y
    • If a free fails:
        o Thread b failed to free memory on line y
    • If no error occurs for a thread’s simulation:
        o Thread a completed successfully: Total allocation operations P, Total
        allocated memory X, Total free operations Q, Total freed memory Y
    Where P, X, Q and Y are the sums for the given thread a over its execution lifetime.
*/
void* run(void *ptr){
    header bucket = (struct Header*) ptr;
    struct Command recvCmd;
    int addr;
    int pass=1;
    int P, X, Q, Y;
    P=X=Q=Y=0;

    cmd_p cmd;
    while(readCommand(bucket, &recvCmd)){
        cmd = cloneCmd(&recvCmd);
        if(cmd->type==ALLOCATE){
            addr = new(bucket, cmd->size);
            //printf("%d : T%d A%d - addr=%d\n", cmd->lineNum, bucket->id, cmd->arg, addr);
            if(addr==-1){
                printf("Thread %d failed to allocate %d bytes of memory on line %d\n", bucket->id, cmd->size, cmd->lineNum);
                free(cmd);
                pass=0;
                break;
            }
            cmd->addr = addr;
            P++;  X += cmd->size;   //allocation stats.
            addCommand(bucket, cmd);
        }else if(cmd->type==FREE){
            //printf("%d : T%d F%d\n", cmd->lineNum, bucket->id, cmd->arg);
            cmd_p cmdAlloc = findCommand(bucket, cmd->arg); //cmd->arg is lineNum for corresponding alloc cmd.
            cmd->addr = cmdAlloc->addr;
            cmd->size = cmdAlloc->size;
            if(!delete(bucket, cmd->addr, cmd->size)){
                printf("Thread %d failed to free memory on line %d\n", bucket->id, cmd->arg);
                free(cmd);
                pass=0;
                break;
            }
            Q++;  Y += cmd->size;   //free stats.
        }else{
            printf("%d : T%d Invalid command.\n", cmd->lineNum, bucket->id);
            free(cmd);
            pass=0;
            break;
        }
        if(debug)
            printFreeList(bucket);
    }

    close(bucket->pipe[0]);
    if(pass)
        printf("Thread %d completed successfully: Total allocation operations %d, Total allocated memory %d, Total free operations %d, Total freed memory %d\n",bucket->id, P, X, Q, Y);

    freeCommands(bucket);
    freeHeap(bucket);
    pthread_exit(NULL);
}

int usage(char *program){
    fprintf(stderr, "Usage: %s -s <heap-size> -[fbw] <trace-file> [-d]\n", program);
    fprintf(stderr, "\t-f : first fit\n");
    fprintf(stderr, "\t-b : best fit\n");
    fprintf(stderr, "\t-w : worst fit\n");
    fprintf(stderr, "\t-d : debug output\n");
    exit(1);
}

int main(int argc, char *argv[]) {
    if (argc<5)
        usage(argv[0]);
 
    if(argc==6 && strcmp(argv[5], "-d")==0)
        debug=1;

    heapSize = atoi(argv[2]);
    algorithm = argv[3][1];  //[f]irstfit, [b]estfit, [w]orstfit

    h.down = NULL;
    int lineNum=1;
    int threadId;
    FILE* fp;
    char line[bufSize];

    if ((fp = fopen(argv[4], "r")) == NULL) {
        perror("fopen trace-file");
        return 1;
    }
    int id=0;
    char *token;
    header tmp;
    struct Command sendCmd;
    sendCmd.next = NULL;
    while (fgets(line, sizeof(line), fp) != NULL) {
        sendCmd.lineNum = lineNum++;
        line[strlen(line) - 1] = '\0'; // eat the newline fgets() stores
        token = strtok(line, " ");
        while (token != NULL){
            switch(token[0]){
                case 'T':
                    id = atoi(token+1);
                    break;
                case 'A':
                    sendCmd.type = ALLOCATE;
                    sendCmd.arg  = sendCmd.size = atoi(token+1);
                    break;
                case 'F':
                    sendCmd.type = FREE;
                    sendCmd.arg  = atoi(token+1);
                    break;
                default:
                    break;
            }
            token = strtok(NULL, " ");
        }

        tmp = findHeader(&h, id);
        if(!tmp) {
            pthread_t tid;
            tmp = getHeader(tid, id);  //have ID in header before thread creation.
            int ret = pthread_create(&tid, NULL, run, tmp);
            if(ret != 0) {
                printf("Error: pthread_create() failed\n");
                exit(EXIT_FAILURE);
            }

            tmp->live = 1;
            tmp->tid = tid;

            if(pipe(tmp->pipe) < 0) {
                perror("pipe ");
                exit(1);
            }
            tmp->freeList = getFreeNode(-1, 0); //header for free list.
            freeNode firstFreeNode = getFreeNode(0, heapSize); //header for free list.
            tmp->freeList->next = firstFreeNode;
            addHeader(&h, tmp); //added new header to the list (top of list).
        }
        writeCommand(tmp, &sendCmd);
    }

    tmp = h.down;
    while(tmp){
        close(tmp->pipe[1]);
        pthread_join(tmp->tid, NULL);
        tmp = tmp->down;
    }

    fclose(fp);
    return 0;
}


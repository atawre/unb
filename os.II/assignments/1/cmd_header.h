
typedef struct fnode *freeNode;

typedef struct Command {
    int lineNum;
    int type;
    int arg;
    int addr;
    int size;
    struct Command *next;
}*cmd_p;

typedef struct Header {
    pthread_t tid;
    int id;
    int live;
    int pipe[2];
    freeNode freeList;
    cmd_p next;
    struct Header *down;
}*header;

void addHeader(header h, header tmp){
    tmp->down = h->down;
    h->down = tmp;
}

header findHeader(header h, int id){
    header tmp=h->down; 
    while(tmp){
        if(tmp->id==id)
            return tmp;
        tmp=tmp->down;
    }
    return NULL;
}

cmd_p findCommand(header bucket, int lineNum){
    cmd_p tmp=bucket->next; 
    while(tmp){
        if(tmp->lineNum==lineNum)
            return tmp;
        tmp=tmp->next;
    }
    return NULL;
}

void addCommand(header h, cmd_p cmd){
    //add command to the front.
    cmd->next = h->next;
    h->next = cmd;
}

int readCommand(header h, cmd_p cmd){
    if(read(h->pipe[0], cmd, sizeof(struct Command))<sizeof(struct Command)){
        //perror("read");
        return 0;
    }
        return 1;
}

int writeCommand(header h, cmd_p cmd){
    if(write(h->pipe[1], cmd, sizeof(struct Command))<sizeof(struct Command)){
        perror("write");
        return 0;
    }
        return 1;
}

header getHeader(pthread_t tid, int id){
    header tmp = (struct Header*)malloc(sizeof(struct Header));
    tmp->live = 0;
    tmp->tid = tid;
    tmp->id = id;
    tmp->freeList = NULL;
    tmp->next = NULL;
    tmp->down = NULL;
    return tmp;
}

cmd_p getCmdNode(int lineNum){
    cmd_p tmp = (struct Command*)malloc(sizeof(struct Command));
    tmp->lineNum = lineNum;
    tmp->next = NULL;
    return tmp;
}

cmd_p cloneCmd(cmd_p cmd){
    cmd_p tmp = (struct Command*)malloc(sizeof(struct Command));
    tmp->lineNum = cmd->lineNum;
    tmp->type = cmd->type;
    tmp->arg = cmd->arg;
    tmp->addr = cmd->addr;
    tmp->size = cmd->size;
    tmp->next = cmd->next;
    return tmp;
}

void freeCommands(header bucket){
    cmd_p cmd, tmp;
    cmd = bucket->next;
    while(cmd){
        tmp = cmd->next;
        free(cmd);
        cmd = tmp;
    }
    bucket->next = NULL;
}


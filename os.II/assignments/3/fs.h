//
// Created by ataware on 25/3/18.
//

#ifndef FS_FS_H
#define FS_FS_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <math.h>
#include <assert.h>
#include <sys/types.h>
#include <libgen.h>

#define SUCCESS 0
#define FAILURE 1

//flags
#define SUPER_READ 0
#define SUPER_WRITE 1
#define FILE_READ 0
#define FILE_WRITE 1
#define DIR_READ 3
#define DIR_WRITE 4

extern int numblocks;

typedef struct superblock{
    char name[255];
    char flags; /* 0 – read; 1 – write */
    int num_blocks; /* number of blocks in the partition */
    int root_block; /* block number of the root inode */
    int num_free_blocks; /* number of blocks that are free */
    char block_map[756]; /* bit map of used(1)/free(0) blocks */
}sblock_t;

typedef struct inode{
    char filename[255];
    char flags; /* file: 0–read; 1–write; dir: 3–read; 4-write*/
    int file_size; /* number of bytes in the file */
    int direct_refs[190]; /* direct references */
    int indirect_ref; /* indirect reference */
}inode_t;


void print_root();

/*
Will allocate the necessary memory for the filesystem (num_blocks * 1K) and initialize the
        first block in the partition as the superblock. It will also create an initial empty directory as
        the root of the partition. The return value is a pointer to the start of the partition in RAM.
*/
void* format(char* name, char flags, int num_blocks);

/*
Will write the partition to a text file named text_file. The partition is to be dumped 1
character at a time so as to avoid any endian issues. Upon completion, return SUCCESS or
FAILURE.
*/
int dump_to_disk(void* partition, char* text_file);

/*
Will read/load the partition from a text file named text_file. The partition is to be read
1 character at a time so as to avoid any endian issues. The return value is the start of the
partition in RAM.
*/
void* load_from_disk(char* text_file);

/*
Will create an inode for the directory as specified by the parameters. Note: The name will
specify the absolute path to the new directory. For example, “/dev/new_directory” will
        create “new_directory” in the sub-directory “dev” from the root “/” directory. If a directory
        along the path does not exist, then the call to mkdir fails and 1 is returned. The
        new_directory is created containing 0 files and with the flags specified. Note: You must
        pay attention to the flags! If a sub-directory on the path is not-writable, then the call to
mkdir fails! Upon completion, return SUCCESS or FAILURE.
*/
int mkdir(char* name, char flags);

/*
  Will remove the inode for the directory specified by the name parameter. If the directory
  is not empty, the directory cannot be deleted and a 1 is returned. Upon completion, return
SUCCESS or FAILURE.
*/
int rmdir(char* name);


/*
Will copy the file specified by local_file into the partition as the file name with the
flags as specified. The name will specify the absolute path to the new file. For example,
“/dev/new_file” will create “new_file” in the sub-directory “dev” from the root “/”
directory. After creating the inode, it will copy the contents (character by character) into
        the partition creating new data blocks as needed. If there are not enough free blocks to
copy the file then the system call fails with no blocks used and 1 is returned. Upon
        completion, return SUCCESS or FAILURE.
*/
int copy_file(char* name, char flags, char* local_file);

/*Will remove the inode and all associated data blocks for the file specified by the name
parameter. The name will specify the absolute path to the file. If the file does not exist, the
file cannot be deleted and FAILURE is returned. Upon completion, return SUCCESS or
FAILURE. */
int remove_file(char* name);

/*Will print the contents of the file specified by name. The name will specify the absolute
path to the file. If the file does not exist then nothing is printed and 0 is returned.
Otherwise, the file contents are printed character by character to stdout and the number of
characters printed are returned.*/
int print_file(char* name);

#endif //FS_FS_H

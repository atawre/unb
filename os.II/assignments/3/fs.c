#include "fs.h"

sblock_t *sb=0;
unsigned bsize = 1024;
unsigned max_blocks = 6048;
unsigned min_blocks = 32;
unsigned start_datablock=1;

long getFileSize(char *file){
    long fileSize = 0;
    FILE* fp = fopen(file, "r");
    if(fp) {
        fseek(fp, 0 , SEEK_END);
        fileSize = ftell(fp);
        fseek(fp, 0 , SEEK_SET);// needed for next read from beginning of file
        fclose(fp);
    }
    return(fileSize);
}

void set(void *block, int bitnum){
    char *byte = (char *)block;
    byte += bitnum/8;
    *byte |= 0x1 << (bitnum%8);
    sb->num_free_blocks--;
}

void unset(void *block, int bitnum){
    char *byte = (char *)block;
    byte += bitnum/8;
    *byte &= ~(0x1 << (bitnum%8));
    sb->num_free_blocks++;
}

int is_set(void *block, int bitnum){
    char *byte = (char *)block;
    byte += bitnum/8;
    return(*byte & (0x1 << (bitnum%8)));
}

void markfree(int blknum){
    unset(sb->block_map, blknum);
    memset(sb+blknum, 0, bsize);
}

int alloc_b(){
    int i=0;
    for(i=0; i<max_blocks; i++) {
        if (!is_set(sb->block_map, i))
            break;
    }
    if(i==max_blocks)
        return -1;
    set(sb->block_map, i);
    memset((void *)(sb+i), 0, bsize);
    return i;
}

int ialloc(char *name, char flags){
    int i,j;
    for(i=0; i<max_blocks; i++) {
        if (!is_set(sb->block_map, i))
            break;
    }
    if(i==max_blocks)
        return -1;
    set(sb->block_map, i);
    inode_t *inode = (inode_t*)(sb+i);
    strcpy(inode->filename, name);
    inode->file_size=0;
    inode->flags = flags;
    for(j=0; j<190; j++) {
        inode->direct_refs[j] = -1;
    }
    inode->indirect_ref = -1;
    return i;
}

int isvalid_block(int block){
    if(block > 0 && block < numblocks)
        return 1;
    return 0;
}

/*
Will allocate the necessary memory for the filesystem (num_blocks * 1K) and initialize the
        first block in the partition as the superblock. It will also create an initial empty directory as
        the root of the partition. The return value is a pointer to the start of the partition in RAM.
*/
void* format(char* name, char flags, int num_blocks){
    assert((num_blocks>31 && num_blocks<6049));
    void *memory = malloc(num_blocks*1024);
    if(!memory){
        perror("malloc");
        exit(1);
    }

    bzero(memory, num_blocks*1024);

    sb = (sblock_t *)memory;
    strcpy(sb->name, name);
    sb->flags = flags;
    sb->num_blocks = num_blocks;
    set(sb->block_map, 0);   //superblock
    sb->root_block = ialloc("/", DIR_WRITE);
    sb->num_free_blocks = num_blocks-2;
    return memory;
}

/*
Will write the partition to a text file named text_file. The partition is to be dumped 1
character at a time so as to avoid any endian issues. Upon completion, return SUCCESS or
FAILURE.
*/
int dump_to_disk(void* partition, char* text_file){
    int fd = open(text_file, O_RDWR|O_TRUNC|O_CREAT, 0644);
    if(fd < 0){
        perror("open");
        exit(1);
    }

    char *head = (char *)partition;
    unsigned int bytes = 1024*numblocks;
    for(int i=0; i<bytes; i++) {
        if (1 != write(fd, head, 1)) {
            perror("write");
            close(fd);
            return FAILURE;
        }
        head++;
    }
    close(fd);
    return SUCCESS;
}

/*
Will read/load the partition from a text file named text_file. The partition is to be read
1 character at a time so as to avoid any endian issues. The return value is the start of the
partition in RAM.
*/
void* load_from_disk(char* text_file){
    int fd = open(text_file, O_RDONLY);
    if(fd < 0){
        perror("open");
        exit(1);
    }

    long size  = getFileSize(text_file);
    numblocks = size/bsize + size%bsize;
    unsigned int bytes = 1024*numblocks;
    void *memory = malloc(bytes);
    if(!memory){
        perror("malloc");
        exit(1);
    }

    char *head = (char *)memory;
    for(int i=0; i<bytes; i++) {
        if (1 != read(fd, head, 1)) {
            perror("read");
            close(fd);
            exit(1);
        }
        head++;
    }
    sb = (inode_t *)memory;
    close(fd);
    sb = (inode_t *)memory;
    return memory;
}

void print_root(){
    inode_t *root = (inode_t *)(sb+1);
    for(int i=0; i<190; i++){
        inode_t *inode = (inode_t *)(sb+root->direct_refs[i]);
        printf("\n%s", inode->filename);
    }
}

int iget(char *name, int dir){
    int i=0;
    inode_t *dnode = (inode_t*)(sb+dir);
    inode_t *inode=0;
    for(i=0; i<190; i++) {
        if(isvalid_block(dnode->direct_refs[i])) {
            inode = (inode_t*)(sb + dnode->direct_refs[i]);
            if(0==strcmp(inode->filename, name))
                return dnode->direct_refs[i];
        }
    }

    if(!isvalid_block(dnode->indirect_ref))
        return -1;

    int *iblock = (int *)(sb + dnode->indirect_ref);

    for(i=0; i<256; i++) {
        if(isvalid_block(iblock[i])) {
            inode = sb + iblock[i];
            if(0==strcmp(inode->filename, name))
                return iblock[i];
        }
    }
    return -1;
}

int iput(int dir, int inum){
    int i=0;
    inode_t *dnode = sb+dir, *inode;
    if(dnode->flags==DIR_READ){
        printf("\n'%s' is read only.", dnode->filename);
        return FAILURE;
    }

    for(i=0; i<190; i++) {
        if(!isvalid_block(dnode->direct_refs[i])) {
            dnode->direct_refs[i] = inum;
            dnode->file_size += 4;
            return SUCCESS;
        }
    }

    if(!isvalid_block(dnode->indirect_ref)){
        int indirect = alloc_b();
        if(!isvalid_block(indirect))
            return FAILURE;
        dnode->indirect_ref = indirect;
    }

    int *iblock = (int *)(sb + dnode->indirect_ref);

    for(i=0; i<256; i++) {
        if(!isvalid_block(iblock[i])) {
            iblock[i] = inum;
            dnode->file_size += 4;
            return SUCCESS;
        }
    }
    return FAILURE;
}

/*
Will create an inode for the directory as specified by the parameters. Note: The name will
specify the absolute path to the new directory. For example, “/dev/new_directory” will
        create “new_directory” in the sub-directory “dev” from the root “/” directory. If a directory
        along the path does not exist, then the call to mkdir fails and 1 is returned. The
        new_directory is created containing 0 files and with the flags specified. Note: You must
        pay attention to the flags! If a sub-directory on the path is not-writable, then the call to
mkdir fails! Upon completion, return SUCCESS or FAILURE.
*/

int mkdir(char* name, char flags) {
    if(sb->flags==SUPER_READ){
        printf("\nmkdir:read only file system.");
    }

    char *path = strdup(name);
    char *token = strtok(path, "/");
    int current_root = sb->root_block, next_root;
    inode_t *cur_dir = 0;
    while (token) {
        cur_dir = (inode_t*)(sb+current_root);
        if(cur_dir->flags==DIR_READ)
            return FAILURE;
        next_root = iget(token, current_root);
        token = strtok(NULL, "/");
        if (-1 == next_root)
            break;
        current_root = next_root;
    }

    if (-1 == next_root && token) {
        printf("\nInvalid path.");
        return FAILURE;
    }

    free(path);
    //this is last component.
    path = strdup(name);
    token = basename(path);
    int inode = ialloc(token, flags);
    if (inode == -1){
        printf("\nInsufficient free blocks.");
        return FAILURE;
    }
    free(path);
    if(FAILURE==iput(current_root, inode)){
        markfree(inode);
        return FAILURE;
    }
    return SUCCESS;
}

int remove_last(inode_t *inode){
    int i=0, last=0, found=0;
    int icount = inode->file_size/4;

    if(icount/190){
        int *iblock = (int *) (sb + inode->indirect_ref);
        last = iblock[icount-191];
        iblock[icount-191] = -1;
    }else{
        last = inode->direct_refs[icount-1];
        inode->direct_refs[icount-1] = -1;
    }

    return last;
}

int get_slot(inode_t *inode, int dirent){
    int slot=-1, i=0;
    for(i=0; i<190; i++) {
        if(inode->direct_refs[i]==dirent) {
            slot = i;
            break;
        }
    }

    if(isvalid_block(inode->indirect_ref) && slot==-1) {
        int *iblock = (int *) (sb + inode->indirect_ref);
        for (i = 0; i < 256; i++) {
            if (iblock[i]==dirent) {
                slot = i+190;
                break;
            }
        }
    }
    return slot;
}

/*
  Will remove the inode for the directory specified by the name parameter. If the directory
  is not empty, the directory cannot be deleted and a 1 is returned. Upon completion, return
SUCCESS or FAILURE.
*/

int rmdir(char* name){
    if(sb->flags==SUPER_READ){
        printf("\nrmdir:read only file system.");
    }

    char *path = strdup(name);
    char *base = basename(strdup(name));
    inode_t *dnode=0;
    int i=0;

    char *token = strtok(path, "/");
    int current_root = sb->root_block, next_root;
    while (token) {
        next_root = iget(token, current_root);
        if (-1 == next_root) {
            printf("\nMissing directory.");
            return FAILURE;
        }
        dnode = (inode_t*)(sb+next_root);
        if(0==strcmp(dnode->filename, base))
            break;
        current_root = next_root;
        token = strtok(NULL, "/");
    }

    for(i=0; i<190; i++) {
        if(isvalid_block(dnode->direct_refs[i])) {
            printf("\nDirectory not empty.");
            return FAILURE;
        }
    }

    if(isvalid_block(dnode->indirect_ref)) {
        int *iblock = (int *) (sb + dnode->indirect_ref);
        for (i = 0; i < 256; i++) {
            if (isvalid_block(iblock[i])) {
                printf("\nDirectory not empty.");
                return FAILURE;
            }
        }
        markfree(dnode->indirect_ref);
        dnode->indirect_ref = -1;
    }

    markfree(next_root);

    inode_t *pnode = (inode_t*)(sb+current_root);
    int slot = get_slot(pnode, next_root);
    if(slot==-1){
        printf("\nIt should never fail at this stage.");
        return FAILURE;
    }

    int last = remove_last(pnode);
    if(slot/190 > 0){
        int *iblock = (int *) (sb + pnode->indirect_ref);
        if(last==next_root) //deleting the last entry.
            iblock[slot-190] = -1;
        else
            iblock[slot-190] = last;
    }else{
        if(last==next_root)     //deleting the only entry.
            pnode->direct_refs[slot] = -1;
        else
            pnode->direct_refs[slot] = last;
    }
    pnode->file_size -= 4;
    return SUCCESS;
}

/*
Will copy the file specified by local_file into the partition as the file name with the
flags as specified. The name will specify the absolute path to the new file. For example,
“/dev/new_file” will create “new_file” in the sub-directory “dev” from the root “/”
directory. After creating the inode, it will copy the contents (character by character) into
        the partition creating new data blocks as needed. If there are not enough free blocks to
copy the file then the system call fails with no blocks used and 1 is returned. Upon
        completion, return SUCCESS or FAILURE.
*/
int copy_file(char* name, char flags, char* local_file){
    if(sb->flags==SUPER_READ){
        printf("\ncopy:read only file system.");
    }

    int blocks_needed=0;
    long size = getFileSize(local_file);
    blocks_needed = size/bsize + 1;  // +1 for inode.
    if(size%bsize)
        blocks_needed++;

    if(sb->num_free_blocks < blocks_needed || blocks_needed > 447){
        printf("\ncopy:insufficient free blocks or file too big.");
        return FAILURE;
    }

    char *path = strdup(name);
    char *token = strtok(path, "/");
    int current_root = sb->root_block, next_root;
    inode_t *cur_dir = 0;
    while (token) {
        cur_dir = (inode_t*)(sb+current_root);
        if(cur_dir->flags==DIR_READ)
            return FAILURE;
        next_root = iget(token, current_root);
        token = strtok(NULL, "/");
        if (-1 == next_root)
            break;
        current_root = next_root;
    }

    if (-1 == next_root && token) {
        printf("\ncopy:invalid path.");
        return FAILURE;
    }

    free(path);
    //this is last component.
    path = strdup(name);
    token = basename(path);
    int inum = ialloc(token, flags);
    if (inum == -1){
        printf("\ncopy:insufficient free blocks.");
        return FAILURE;
    }
    free(path);

    if(iput(current_root, inum)==FAILURE){
        markfree(inum);
        return FAILURE;
    }

    int fd = open(local_file, O_RDONLY);
    if(fd<0){
        perror("open");
        return FAILURE;
    }

    inode_t *inode = (inode_t *)(sb+inum);
    inode->file_size = size;
    inode->flags = flags;
    int i=0, block, nbytes;
    for(i=0; i<190; i++) {
        block = alloc_b();
        nbytes = read(fd, (void*)(sb+block), bsize);
        if(nbytes<=0) {
            markfree(block);
            break;
        }
        inode->direct_refs[i] = block;
        size -= nbytes;
        if(nbytes<bsize)
            break;
    }

    if(size>0){
        inode->indirect_ref = alloc_b();
        int *iblock = (int *)(sb + inode->indirect_ref);
        for(i=0; i<256; i++) {
            iblock[i] = alloc_b();
            nbytes = read(fd, (void*)(sb+iblock[i]), bsize);
            if(nbytes==-1) {
                markfree(iblock[i]);
                break;
            }
            size -= nbytes;
            if(nbytes<bsize)
                break;
        }
    }

    close(fd);
    return SUCCESS;  //check if size is zero.
}


/*Will remove the inode and all associated data blocks for the file specified by the name
parameter. The name will specify the absolute path to the file. If the file does not exist, the
file cannot be deleted and FAILURE is returned. Upon completion, return SUCCESS or
FAILURE. */
int remove_file(char* name){
    if(sb->flags==SUPER_READ){
        printf("\nremove:read only file system.");
    }

    char *path = strdup(name);
    char *base = basename(strdup(name));
    inode_t *inode=0;
    int i=0;

    char *token = strtok(path, "/");
    int current_root = sb->root_block, next_root;
    while (token) {
        next_root = iget(token, current_root);
        if (-1 == next_root) {
            printf("\nremove:missing file.");
            return FAILURE;
        }
        inode = (inode_t*)(sb+next_root);
        if(0==strcmp(inode->filename, base))
            break;
        current_root = next_root;
        token = strtok(NULL, "/");
    }

    for(i=0; i<190; i++) {
        if(isvalid_block(inode->direct_refs[i])) {
            markfree(inode->direct_refs[i]);
            inode->direct_refs[i] = -1;
        }
    }

    if(isvalid_block(inode->indirect_ref)) {
        int *iblock = (int *) (sb + inode->indirect_ref);
        for (i = 0; i < 256; i++) {
            if (isvalid_block(iblock[i])) {
                markfree(iblock[i]);
                iblock[i] = -1;
            }
        }
        markfree(inode->indirect_ref);
        inode->indirect_ref = -1;
    }

    markfree(next_root);

    inode_t *pnode = (inode_t*)(sb+current_root);
    int slot = get_slot(pnode, next_root);
    if(slot==-1){
        printf("\nIt should never fail at this stage.");
        return FAILURE;
    }

    int last = remove_last(pnode);
    if(slot/190 > 0) {
        int *iblock = (int *) (sb + pnode->indirect_ref);
        if (last == next_root){ //deleting the last entry.
            assert(slot==255);
            iblock[slot - 190] = -1;
        }else {
            iblock[slot - 190] = last;
        }
    }else{
        if(last==next_root) {     //deleting the only entry.
            assert(slot==0);
            pnode->direct_refs[slot] = -1;
        }else {
            pnode->direct_refs[slot] = last;
        }
    }
    pnode->file_size -= 4;
    return SUCCESS;
}

/*Will print the contents of the file specified by name. The name will specify the absolute
path to the file. If the file does not exist then nothing is printed and 0 is returned.
Otherwise, the file contents are printed character by character to stdout and the number of
characters printed are returned.*/
int print_file(char* name){
    char *path = strdup(name);
    char *base = basename(strdup(name));
    inode_t *inode=0;
    int i=0;

    char *token = strtok(path, "/");
    int current_root = sb->root_block, next_root;
    while (token) {
        next_root = iget(token, current_root);
        if (-1 == next_root) {
            printf("\nprint:missing file.");
            return 0;
        }
        inode = (inode_t*)(sb+next_root);
        if(0==strcmp(inode->filename, base))
            break;
        current_root = next_root;
        token = strtok(NULL, "/");
    }

    void *data=0;
    int nbytes=0;
    int left = inode->file_size;
    for(i=0; i<190; i++) {
        if(isvalid_block(inode->direct_refs[i])) {
            data = sb + inode->direct_refs[i];
            if(left/bsize) {
                nbytes = write(1, data, bsize);
                assert(nbytes==bsize);
                left -= nbytes;
            }else{
                nbytes = write(1, data, left);
                assert(nbytes==left);
                left -= nbytes;
            }
        }
    }

    if(isvalid_block(inode->indirect_ref) && left>0) {
        int *iblock = (int *) (sb + inode->indirect_ref);
        for (i = 0; i < 256; i++) {
            if (isvalid_block(iblock[i])) {
                data = sb + iblock[i];
                if(left/bsize) {
                    nbytes = write(1, data, bsize);
                    assert(nbytes==bsize);
                    left -= nbytes;
                }else{
                    nbytes = write(1, data, left);
                    assert(nbytes==left);
                    left -= nbytes;
                }
            }
        }
    }

    return inode->file_size;
}

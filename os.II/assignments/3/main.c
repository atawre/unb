#include <stdio.h>
#include "fs.h"

int numblocks=6048;
char path[255];
extern sblock_t *sb;

int main() {
    void *ramdisk=0;

    ramdisk = format("test", SUPER_WRITE, numblocks);
    for(int i=0; i<446; i++){
        sprintf(path, "/%d", i);
        mkdir(path, 4);
        sprintf(path, "/%d/passwd", i);
        copy_file(path, 2, "/etc/passwd");
    }


//    dump_to_disk(ramdisk, "ramdisk");
//    free(ramdisk);
//    ramdisk = load_from_disk("ramdisk");
    print_file("/445/passwd");
    remove_file("/445/passwd");
    copy_file("/445/passwd", 2, "/etc/passwd");
    assert(FAILURE==copy_file("/passwd", 2, "/etc/passwd"));
    assert(FAILURE==mkdir("/test", 2));

    return 0;
}

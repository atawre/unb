#!/bin/bash

SUITES="avrora fop h2 jython luindex lusearch pmd sunflow xalan "

mkdir -p output
mkdir -p images
mkdir -p images.outlier.included

declare -a stats=($(ls STATS))

for suite in $SUITES
do

    for stat in ${stats[@]}
    do
        cat STATS/$stat/dacapo.$suite | grep PASSED |awk '{print $7}' > $stat
    done
    paste ${stats[@]} | tail -50 > output/$suite
    rm -f ${stats[@]}

    #set xtics ("Protect" 1, "EdenSkip.noProfile" 2, "EdenSkip.Profile" 3) scale 0.0

    count=0
    names=
    for s in ${stats[@]}
    do
        count=$((count+1))
        names="$names \"$s\" $count, "
    done
    names=`echo $names | sed 's/,$//'`

	gnuplot <<EOF
        set term png
        set style fill solid 0.5 border -1
        set style boxplot fraction 0.99
        set style data boxplot
        set boxwidth  0.5
        set pointsize 0.5
        set datafile separator "\t"
        unset key
        set border 2
        set xtics ($names) scale 0.0
        set xtics nomirror
        set ytics nomirror
        set ylabel "Average Runtime (ms)"
        set xlabel "minMem=1536m maxMem=1536m balanced coldMem=1536m"
        set output "images.outlier.included/$suite.png"
        set title "$suite Benchmarks (50 runs per configuration)"
        plot for [i=1:$count] 'output/$suite' using (i):i notitle
        set output
EOF
	gnuplot <<EOF
        set term png
        set style fill solid 0.5 border -1
        set style boxplot outliers pointtype 7
        set style data boxplot
        set boxwidth  0.5
        set pointsize 0.5
        set datafile separator "\t"
        unset key
        set border 2
        set xtics ($names) scale 0.0
        set xtics nomirror
        set ytics nomirror
        set ylabel "Average Runtime (ms)"
        set xlabel "minMem=1536m maxMem=1536m balanced coldMem=1536m"
        set output "images/$suite.png"
        set title "$suite Benchmarks (50 runs per configuration)"
        plot for [i=1:$count] 'output/$suite' using (i):i notitle
        set output
EOF

done
